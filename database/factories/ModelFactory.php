<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Services::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'status' => $faker->numberBetween($min = 0, $max = 1)
    ];
});

$factory->define(App\Countries::class, function (Faker\Generator $faker) {
    return [
        'country' => $faker->country,
        'image' => $faker->image($dir = '/tmp', 150, 150),
        'title' => $faker->title,
        'description' => $faker->paragraph,
        'keywords' => $faker->sentence(10)
    ];
});

$factory->define(App\States::class, function (Faker\Generator $faker) {
    return [
        'country_id' => factory(\App\Countries::class)->create()->id,
        'name' => $faker->city,
        'title' => $faker->sentence,
        'description' => $faker->paragraph,
        'keywords' => $faker->sentence(10)
    ];
});

$factory->define(App\Cities::class, function (Faker\Generator $faker) {
    return [
        'country_id' => factory(App\Countries::class)->create()->id,
        'state_id' => factory(App\States::class)->create()->id,
        'name' => $faker->city,
        'title' => $faker->sentence,
        'description' => $faker->paragraph,
        'keywords' => $faker->sentence(10)
    ];
});

$factory->define(App\PinCodes::class, function (Faker\Generator $faker) {
    return [
        'country_id' => factory(App\Countries::class)->create()->id,
        'state_id' => factory(App\States::class)->create()->id,
        'city_id' => factory(App\Cities::class)->create()->id,
        'pincode' => $faker->numberBetween($min = 0, $max = 9999)
    ];
});

$factory->define(App\branches::class, function (Faker\Generator $faker) {
    return [
        'city_id' => factory(App\Cities::class)->create()->id,
        'person' => $faker->name,
        'city_id' => $faker->address,
        'contact_number' => $faker->phoneNumber,
        'email' => $faker->email
    ];
});

$factory->define(App\Customers::class, function (Faker\Generator $faker) {
    return [
        'country_id' => factory(App\Countries::class)->create()->id,
        'city_id' => factory(App\Cities::class)->create()->id,
        'name' => $faker->name,
        'company' => $faker->company,
        'zip' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'email' => $faker->email,
        'customer_commission' => $faker->numberBetween($min = 0, $max = 30),
        'password' => bcrypt($faker->password($min = 6, $max = 30))
    ];
});