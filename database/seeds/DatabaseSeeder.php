<?php

use App\Customers;
use Illuminate\Database\Seeder;
use \App\Countries;
use \App\Cities;
use \App\PinCodes;
use \App\States;
use \App\branches;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        PinCodes::truncate();
        // $this->call(UsersTableSeeder::class);
//        Countries::truncate();
//        factory(Countries::class,20)->create();
//       factory(\App\States::class,20)->create();
//        factory(\App\Cities::class, 20)->create();
        factory(\App\PinCodes::class, 20)->create();
//        factory(branches::class, 20)->create();
        factory(Customers::class,20)->create();
    }
}
