<style type="text/css">
    ul {
        list-style: none;
    }

    li {
        text-align: left;
    }
</style>
<div class="col-xs-12">
    <div class="errors alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li> {{ $error }} </li>
            @endforeach
        </ul>
    </div>
</div>