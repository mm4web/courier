@extends('panel.template')
@section('content')

    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Add Customer
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">

                @if($errors->any())
                    @include('errors.list')
                @endif

                @if(Session::has('new_customer'))
                    <div class="alert alert-success"> {{ session('new_customer') }} </div>
                @endif
                {!! Form::open() !!}

                <div class="form-group">
                    {!! Form::label('name',' name') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('company','company') !!}
                    {!! Form::text('company',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('zip','zip') !!}
                    {!! Form::text('zip',null,['class'=>'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('country','country') !!}
                    <select name="country_id" id="country" class="form-control"
                            data-link="{{ url('panel/locations/getCity') }}">
                        <option value="1">--select country</option>
                        @foreach($countries as $country)
                            <option value="{{ $country->id }}"> {{ $country->country }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    {!! Form::label('city','city') !!}
                    <select name="city_id" id="cities" class="form-control">
                        <option value="">--select city</option>
                    </select>
                </div>

                <div class="form-group">
                    {!! Form::label('phone','phone') !!}
                    {!! Form::input('number','phone',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('address','address') !!}
                    {!! Form::text('address',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email','email') !!}
                    {!! Form::email('email',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('customer_commission','customer commission:') !!}
                    {!! Form::input('number','customer_commission',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('password','password') !!}
                    <input type="password" name="password" id="password" class="form-control">
                </div>

                <div class="form-group">
                    {!! Form::label('confirmation_password','password confirmation') !!}
                    <input type="password" name="password_confirmation" id="password_conformation" class="form-control">
                </div>

                {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}

                {!! form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script type="text/javascript">
        $('#country').change(function () {
            var url = $(this).data('link');
            var city = $('#cities');
            var value = $(this).val();
            getCity(url, city, value);
        })
    </script>
@stop