@extends('panel.template')

@section('content')
    <!--Custom Toolbar-->

    <style>
        .edit {
            float: left;
            margin-right: 5px;
        }

        .btn {
            margin-bottom: 5px;
        }
    </style>
    <!--===================================================-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b> show customers </b></h4>
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true">#</th>
                        <th data-field="uniqueId" data-sortable="true">UniqueId</th>
                        <th data-field="Name" data-sortable="true">Name</th>
                        <th data-field="phone" data-sortable="true">phone</th>
                        <th data-field="email" data-sortable="true">email</th>
                        <th data-field="action" data-sortable="true">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($customers as $count => $customer)
                        <tr>
                            <td>{{ $count }}</td>
                            <td>{{ $customer->id }}</td>

                            <td>{{ $customer->name }}</td>
                            <td> {{ $customer->phone }}</td>
                            <td> {{ $customer->email }} </td>
                            <td class="action">

                                <a href="{{ url('panel/customer/edit/'.$customer->id) }}" class=" btn-circle btn btn-sm
                                btn-primary
                                edit"> <i
                                            class=" fa
                            fa-pencil-square-o"></i></a>
                                {!! Form::open() !!}
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fa
                                fa-trash-o"></i></button>
                                {!! Form::close() !!}
                                <a href="#" title="payment details" class="  btn btn-sm btn-info"> <i
                                            class="fa fa-money"></i>
                                </a>
                                <a href="#" class=" btn-circle btn btn-sm btn-warning" title="booking details"> <i
                                            class="fa
                                fa-exclamation-circle"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        var resizefunc = [];
    </script>
@stop