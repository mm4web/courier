@extends('panel.template')
@section('content')
    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Add branch
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">
                @if($errors->any())
                    @include('errors.list')
                @endif
                @if(Session::has('new_branch'))
                    <div class="alert alert-success"> {{ session('new_branch') }} </div>
                @endif
                {!! Form::open() !!}
                <div class="form-group">
                    {!! Form::label('city_id','branch city') !!}
                    <select name="city_id" id="city" class="form-control">
                        <option value=""> --select city--</option>
                        @foreach($cities as $city)
                            <option value="{{ $city->id }}"> {{ $city->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="status"> status </label>
                    <select name="status" id="status" class="form-control">
                        <option value="1">active</option>
                        <option value="0"> unactive</option>
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('person','branch person') !!}
                    {!! Form::text('person',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('address','address') !!}
                    {!! Form::text('address',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('contact_number','contact number') !!}
                    {!! Form::text('contact_number',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email','email') !!}
                    {!! Form::email('email',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop