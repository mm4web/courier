@extends('panel.template')
@section('content')

    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Add Agent
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">

                @if($errors->any())
                    @include('errors.list')
                @endif

                @if(Session::has('new_agent'))
                    <div class="alert alert-success"> {{ session('new_agent') }} </div>
                @endif

                @if(Session::has('update_agent'))
                    <div class="alert alert-success"> {{ session('update_agent') }} </div>
                @endif
                {!! Form::model($agent) !!}

                <div class="form-group">
                    {!! Form::label('name','agent name') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('address','Address') !!}
                    {!! Form::text('address',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('phone','phone') !!}
                    {!! Form::input('number','phone',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('fax','Fax') !!}
                    {!! Form::input('number','fax',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email','email') !!}
                    {!! Form::email('email',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('commission','agent Commission') !!}
                    {!! Form::input('number','commission',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('country_id','country') !!}
                    <select name="country_id" id="countries" data-link="{{ url('panel/locations/getBranch') }}"
                            class="form-control">
                        <option value="">--select--</option>
                        @foreach($countries as $country)
                            <?php $selected = "" ?>
                            @if($country->id == $agent->country_id)
                                <?php $selected = "selected" ?>
                            @endif
                            <option value="{{ $country->id }}" {{ $selected  }} > {{ $country->country }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    {!! Form::label('branch_id','branch location') !!}
                    <select name="branch_id" id="branches" class="form-control">
                        <option value="">--select--</option>
                        @foreach($branches as $branch)
                            <?php $selected = "" ?>
                            @if($branch->id == $agent->branch_id)
                                <?php $selected = "selected" ?>
                            @endif
                            <option value="{{ $branch->id }}" {{ $selected }}>{{ $branch->person }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    {!! Form::label('username','username:') !!}
                    {!! Form::text('username',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('password','password') !!}
                    <input type="password" name="password" id="password" class="form-control">
                </div>

                <div class="form-group">
                    {!! Form::label('confirmation_password','password confirmation') !!}
                    <input type="password" name="password_confirmation" id="password_conformation" class="form-control">
                </div>

                {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}

                {!! form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script type="text/javascript">
        //        $('#countries').change(function () {
        //            var url = $(this).data('link');
        //            var target = "branches";
        //            var country_id = $(this).val();
        //            getBranches(url, target, country_id);
        //        })
    </script>
@stop