@extends('panel.template')
@section('content')
    <style type="text/css">
        .action a {
            border-radius: 50%;
            width: 35px;
            height: 35px;
            margin-bottom: 5px;
        }

    </style>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>show users</b></h4>
                <button class="btn btn-primary"><i class="fa fa-user-plus m-r-5"></i>add Agent</button>
                <button class="btn btn-warning"><i class="fa fa-user-plus m-r-5"></i>add Hub</button>
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true">#</th>
                        <th data-field="name" data-sortable="true">hub/agent</th>
                        <th data-field="phone" data-sortable="true" >phone</th>
                        <th data-field="email" data-sortable="true">email</th>
                        <th data-field="usertype" data-sortable="true" >
                            userType
                        </th>
                        <th data-field="username" data-sortable="true" >username</th>
                        <th data-field="status" data-align="center" data-sortable="true">Branch Location
                        </th>
                        <th data-field="action" data-sortable="true">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>mamsoura</td>
                        <td>12345678</td>
                        <td> ali@yahoo.com </td>
                        <td> <span class="label label-success"> agent </span> </td>
                        <td> Ali </td>
                        <td>egypt</td>
                        <td class="action">
                            <a href="#" class=" btn-circle btn btn-sm btn-primary"> <i class="fa fa-edit"></i> </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-danger"> <i class="fa fa-trash"></i> </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-info"> <i class="fa fa-print"></i> </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-warning"> <i class="fa fa-barcode"></i> </a>
                            <br>
                            <a href="#" class=" btn-circle btn btn-sm btn-success"> <i class="fa fa-file-text-o"></i>
                            </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-success"> <i class="fa  fa-file-pdf-o"></i>
                            </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-success"> <i class="fa fa-repeat"></i> </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-success"> <i class="fa fa-share "></i> </a>
                        </td>
                    </tr>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop