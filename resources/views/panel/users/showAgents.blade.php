@extends('panel.template')
@section('content')
    <style type="text/css">
        .btn {
            float: left;
            margin-right: 5px;
        }
    </style>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b> show agents </b></h4>
                <a href="{{ url('panel/users-management/add_agent') }}" class="btn btn-primary"><i
                            class="fa fa-user-plus m-r-5"></i>add Agent</a>
                <div class="clearfix"></div>
                @if(Session::has('delete_agent'))
                    <div class="alert alert-success"> {{ session('delete_agent') }} </div>
                @endif
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true">#</th>
                        <th data-field="agent" data-sortable="true">agent</th>
                        <th data-field="phone" data-sortable="true">phone</th>
                        <th data-field="email" data-sortable="true">email</th>

                        <th data-field="username" data-sortable="true">username</th>
                        <th data-field="branch" data-align="center" data-sortable="true">Branch Location
                        </th>
                        <th data-field="action" data-sortable="true">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($agents as $count => $agent)
                        <tr>
                            <td>{{ $count }}</td>
                            <td> {{ $agent->name }} </td>
                            <td> {{ $agent->phone }} </td>
                            <td> {{ $agent->email }} </td>
                            <td> {{ $agent->username }} </td>
                            <td>{{ \App\branches::getBranch($agent->branch_id)->person }}</td>
                            <td class="action">
                                <a href="{{ url('panel/agent/edit/'.$agent->id) }}"
                                   class=" btn-circle btn btn-sm btn-primary"> <i class="fa fa-edit"></i> </a>
                                {!! Form::open(['url'=>url('panel/agent/delete/'.$agent->id)]) !!}
                                <button type="submit" class=" btn-circle btn btn-sm btn-danger"><i class="fa
                                    fa-trash"></i></button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop