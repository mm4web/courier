@extends('panel.location_template')
@section('content')

    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Add pin code
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">
                @if($errors->any())
                    @include('errors.list')
                @endif

                @if(Session::has('new_pincode'))
                    <div class="alert alert-success"> {{ session('new_pincode') }} </div>
                @endif

                @if(Session::has('update_pin'))
                    <div class="alert alert-success"> {{ session('update_pin') }} </div>
                @endif
                {!! Form::model($pin) !!}
                <div class="form-group">
                    {!! Form::label('country','Country Name:') !!}
                    <select name="country_id" data-link="{{ url('panel/location/getStates') }}" id="country"
                            class="form-control">
                        <option value=""> -- select country --</option>
                        @foreach($countries as $country)
                            @if($country->id == $pin->country_id)
                                <option value="{{ $country->id }}" selected> {{ $country->country }} </option>
                            @endif

                            <option value="{{ $country->id }}"> {{ $country->country }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('state','select state:') !!}
                    <select name="state_id" data-link="{{ url('panel/location/getCities') }}" id="state"
                            class="form-control">
                        <option value=""> -- select state --</option>
                        @foreach($states as $state)
                            @if($state->id == $pin->state_id)
                                <option value="{{ $state->id }}" selected> {{ $state->name}} </option>
                            @endif
                            <option value="{{ $state->id }}"> {{ $state->name}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('city','select city:') !!}
                    <select name="city_id" id="cities" class="form-control">
                        <option value="">-- select city --</option>
                        @foreach($cities as $city)
                            @if($city->id == $pin->city_id)
                                <option value="{{ $city->id }}" selected> {{ $city->name}} </option>
                            @endif
                            <option value="{{ $city->id }}"> {{ $city->name}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('pincode','Pin Code:') !!}
                    {!! Form::input('number','pincode',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script type="text/javascript">
        $('#country').change(function () {
            var url = $(this).data('link');
            var value = $(this).val();
            $.ajax({
                url: url,
                data: {"country_id": value},
                success: function (data) {
                    $('#state').empty().append('<option value=""> -- select state -- </option>');
                    $(data.data).each(function (data, item) {
                        $('#state').append('<option value="' + item.id + '">' + item.name + '</option>')
                    })
                }
            })
        })

        $('#state').change(function () {
            var url = $(this).data('link');
            var state_id = $(this).val();
            $.ajax({
                url: url,
                data: {"state_id": state_id},
                success: function (data) {
                    $('#cities').empty().append('<option value=""> -- select state -- </option>');
                    $(data.cities).each(function (cities, city) {
                        $('#cities').append('<option value="' + city.id + '">' + city.name + '</option>')
                    })
                }
            })
        })
    </script>
@stop