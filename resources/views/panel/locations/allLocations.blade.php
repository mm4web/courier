@extends('panel.location_template')

@section('content')

    <style type="text/css">
        .edit {
            float: left;
            margin-right: 10px;
        }
    </style>
    <!--Custom Toolbar-->
    <!--===================================================-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b> list all Locations </b></h4>
                @if(Session::has('delete_location'))
                    <p class="text-muted font-13 alert alert-success">
                        {{ session('delete_location') }}
                    </p>
                @endif
                <a href="{{ url('panel/location/add_country') }}" class="btn btn-primary"><i class="fa fa-plus
                m-r-5"></i> Add
                    country</a>
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true"> sr .No</th>
                        <th data-field="country" data-sortable="true">country name</th>
                        <th data-field="flag" data-sortable="true">flag</th>
                        <th data-field="action" data-sortable="true">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($countries as $item => $country)
                        <tr>
                            <td> {{ $item }} </td>
                            <td>  {{ $country->country }} </td>
                            <td class="col-xs-2"><img height="50px" src="{{ asset($country->image) }}"
                                                      alt=""></td>
                            <td class="action">
                                <a href="{{ url('panel/location/edit/'.$country->id) }}" class="edit btn btn-xs
                                btn-info"> <i
                                            class="fa
                                fa-pencil-square"></i> </a>
                                {!! Form::open(['class'=>'form-inline','method'=>'post','url'=>url
                                ('panel/location/delete/'.$country->id)]) !!}
                                <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        var resizefunc = [];
    </script>
@stop