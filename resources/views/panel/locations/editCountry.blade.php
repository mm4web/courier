@extends('panel.location_template')
@section('content')

    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                edit Country
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">
                @if($errors->any())
                    @include('errors.list')
                @endif
                @if(Session::has('new_country'))
                    <div class="alert alert-success"> {{ session('new_country') }} </div>
                @endif

                @if(Session::has('update_location'))
                    <div class="alert alert-success"> {{ session('update_location') }} </div>
                @endif

                {!! Form::model($country,['files'=>true]) !!}
                <div class="form-group">
                    {!! Form::label('country','Country Name:') !!}
                    {!! Form::text('country',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('image','image:') !!}
                    {!! Form::file('image',null,['class'=>'form-control']) !!}
                    <img class="col-xs-2 image" src="{{ asset($country->image) }}" alt="">
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="form-group">
                    {!! Form::label('title','Title:') !!}
                    {!! Form::text('title',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description','description:') !!}
                    {!! Form::textarea('description',null,['class'=>'form-control','rows'=>'5']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('keywords','keywords:  ') !!} <span> split words by (,) </span>
                    {!! Form::textarea('keywords',null,['class'=>'form-control','rows'=>'5']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop
@section('scripts')
    <script type="text/javascript">
        $('#image').change(function () {
            var image = $(this).val();
            $('.image').attr('src',image);
        })
    </script>
@stop