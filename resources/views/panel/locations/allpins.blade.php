@extends('panel.location_template')

@section('content')

    <style type="text/css">
        .action a {
            border-radius: 50%;
            width: 35px;
            height: 35px;
            margin-bottom: 5px;
        }

        .edit {
            float: left;
            margin-right: 15px;
        }
    </style>
    <!--Custom Toolbar-->
    <!--===================================================-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b> list all pincodes </b></h4>
                <p class="text-muted font-13">
                </p>

                @if(Session::has('delete_pincode'))
                    <div class="alert alert-success"> {{ session('delete_pincode') }} </div>
                @endif
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true"> sr .No</th>
                        <th data-field="city" data-sortable="true">city</th>
                        <th data-field="pincode" data-sortable="true">pincode</th>
                        <th data-field="action" data-sortable="true">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($pincodes as $pin => $value)
                        <tr>
                            <td>{{ $pin }}</td>
                            <td> {{ \App\Cities::getCityName($value->city_id) }}</td>
                            <td> {{ $value->pincode }} </td>
                            <td class="action">
                                <a href="{{ url('panel/pincod/edit/'.$value->id) }}" class="btn btn-sm edit"> <i
                                            class="fa
                                fa-pencil-square fa-2x"></i>
                                </a>
                                {!! Form::open(['url'=>url('panel/pincod/delete/'.$value->id)]) !!}
                                <button type="submit" class="btn btn-sm"><i class="fa fa-trash"></i></button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        var resizefunc = [];
    </script>
@stop