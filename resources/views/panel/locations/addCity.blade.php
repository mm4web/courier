@extends('panel.location_template')
@section('content')

    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Add city
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">
                @if($errors->any())
                    @include('errors.list')
                @endif
                @if(Session::has('new_city'))
                    <div class="alert alert-success"> {{ session('new_city') }} </div>
                @endif
                {!! Form::open() !!}
                <div class="form-group">
                    {!! Form::label('country_id','Country Name:') !!}
                    <select name="country_id" data-link="{{ url('panel/location/getStates') }}" id="country"
                            class="form-control">
                        <option value=""> -- select country --</option>
                        @foreach($countries as $country)
                            <option value="{{ $country->id }}"> {{ $country->country }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('state_id','select state:') !!}
                    <select name="state_id" id="state"
                            class="form-control">
                        <option value=""> -- select state --</option>
                        @foreach($states as $state)
                            <option value="{{ $state->id }}"> {{ $state->name}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('name','city Name:') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('title','Title:') !!}
                    {!! Form::text('title',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description','description:') !!}
                    {!! Form::textarea('description',null,['class'=>'form-control','rows'=>'5']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('keywords','keywords:') !!}
                    {!! Form::textarea('keywords',null,['class'=>'form-control','rows'=>'5']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $('#country').change(function () {
            var url = $(this).data('link');
            var value = $(this).val();
            $.ajax({
                url: url,
                data: {"country_id": value},
                success: function (data) {
                    $('#state').empty().append('<option value=""> -- select state -- </option>');
                    $(data.data).each(function (data, item) {
                        $('#state').append('<option value="' + item.id + '">' + item.name + '</option>')
                    })
                }
            })
        })
    </script>
@stop