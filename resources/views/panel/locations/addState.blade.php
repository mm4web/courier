@extends('panel.location_template')
@section('content')

    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Add state
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">
                @if($errors->any())
                    @include('errors.list')
                @endif

                @if(Session::has('new_state'))
                    <div class="alert alert-success"> {{ session('new_state') }} </div>
                @endif
                {!! Form::open() !!}
                <div class="form-group">
                    {!! Form::label('country','Country Name:') !!}
                    <select name="country" id="country" class="form-control">
                        <option value=""> -- select country --</option>
                        @foreach($countries as $country)
                            <option value="{{ $country->id }}">{{ $country->country }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('name','State Name:') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('title','Title:') !!}
                    {!! Form::text('title',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description','description:') !!}
                    {!! Form::textarea('description',null,['class'=>'form-control','rows'=>'5']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('keywords','keywords:') !!}
                    {!! Form::textarea('keywords',null,['class'=>'form-control','rows'=>'5']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop