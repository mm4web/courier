@extends('panel.template')
@section('content')
    <div>
        <div class="portlet">
            <div id="bg-default" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b> Display Un-DELIVERED COD list </b></h4>
                        <p class="text-muted font-13">
                        </p>
                        <table class="table table-striped table-bordered table-hover">
                            <colgroup>
                                <col class="con0">
                                <col class="con1">
                                <col class="con0">
                                <col class="con1">
                                <col class="con0">
                            </colgroup>

                            <thead>
                            <tr>


                            </tr>
                            <tr>
                                <th class="head0">Sr.No.</th>
                                <th class="head1">Awb No.</th>
                                <th class="head0">Date</th>
                                <th class="head1">Destination</th>
                                <th class="head0">Sender</th>
                                <th class="head1">Reciever</th>
                                <th class="head0">Product/Type</th>
                                <th class="head1">Actions</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td class="first style3">1</td>
                                <td>191</td>
                                <td>Sun, 5- Jun -2016</td>
                                <td>Abbot</td>
                                <td>1</td>
                                <td>2</td>
                                <td>Parcel</td>
                                <td class="last" colspan="4" style="text-align:left; padding:10px;">

                                    <a href="http://courierv6.couriersoftwares.com/admin.php?c=shipment&amp;f=add_shipment&amp;id=91&amp;show_type=T"
                                       class="fa fa-pencil-square-o" title="Edit"></a>
                                    <a href="http://courierv6.couriersoftwares.com/admin.php?c=shipment&amp;f=print_shipment&amp;barcode_id=191"
                                       class="fa fa-print" title="Print" target="_new"></a>
                                    <a href="http://courierv6.couriersoftwares.com/admin.php?c=shipment&amp;f=status_update&amp;statu_id=91&amp;cod=cod"
                                       class="fa fa-repeat" title="Status Update"></a>
                                </td>
                            </tr>

                            </tbody>


                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

