<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <link rel="shortcut icon" href="">

    <title> Admin Panel | </title>

    <!--Morris Chart CSS -->
    <link href="{{ asset('assets/panel/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="{{ asset('assets/panel/plugins/morris/morris.css') }}">
    <link href="{{ asset('assets/panel/plugins/sweetalert/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/panel/plugins/bootstrap-table/dist/bootstrap-table.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/panel/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/panel/css/core.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/panel/css/components.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/panel/css/icons.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/panel/css/pages.css') }}" rel="stylesheet" type="text/css"/>

    <link href="{{ asset('assets/panel/css/responsive.css') }}" rel="stylesheet" type="text/css"/>


    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{ asset('assets/panel/js/modernizr.min.js') }}"></script>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="{{ url('/panel') }}" class="logo"> Admin </a>
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">
                    <div class="pull-left">
                        <button class="button-menu-mobile open-left">
                            <i class="ion-navicon"></i>
                        </button>
                        <span class="clearfix"></span>
                    </div>


                    <ul class="nav navbar-nav navbar-right pull-right">
                        <li class="dropdown hidden-xs">
                            <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light"
                               data-toggle="dropdown" aria-expanded="true">
                                <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">3</span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-lg">
                                <li class="notifi-title"><span class="label label-default pull-right">New 3</span>Notification
                                </li>
                                <li class="list-group nicescroll notification-list">
                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-diamond fa-2x text-primary"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">A new order has been placed A new order has
                                                    been placed</h5>
                                                <p class="m-0">
                                                    <small>There are new settings available</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-cog fa-2x text-custom"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">New settings</h5>
                                                <p class="m-0">
                                                    <small>There are new settings available</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-bell-o fa-2x text-danger"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">Updates</h5>
                                                <p class="m-0">
                                                    <small>There are <span class="text-primary font-600">2</span> new
                                                        updates available
                                                    </small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-user-plus fa-2x text-info"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">New user registered</h5>
                                                <p class="m-0">
                                                    <small>You have 10 unread messages</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-diamond fa-2x text-primary"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">A new order has been placed A new order has
                                                    been placed</h5>
                                                <p class="m-0">
                                                    <small>There are new settings available</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>

                                    <!-- list item-->
                                    <a href="javascript:void(0);" class="list-group-item">
                                        <div class="media">
                                            <div class="pull-left p-r-10">
                                                <em class="fa fa-cog fa-2x text-custom"></em>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">New settings</h5>
                                                <p class="m-0">
                                                    <small>There are new settings available</small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="list-group-item text-right">
                                        <small class="font-600">See all notifications</small>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="hidden-xs">
                            <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i
                                        class="icon-size-fullscreen"></i></a>
                        </li>
                        <li class="hidden-xs">
                            <a href="#" class="right-bar-toggle waves-effect waves-light"><i class="icon-settings"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img
                                        src="" alt="user-img"
                                        class="img-circle"> </a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
                                <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
                                <li><a href="javascript:void(0)"><i class="ti-lock m-r-5"></i> Lock screen</a></li>
                                <li><a href="{{ url('logout') }}"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>

                    <li class="text-muted menu-title">Navigation</li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect "><i class="fa fa-cog"></i> <span> General Settings
                            </span>
                        </a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/c_details') }}">Company Details</a></li>
                            <li class="active"><a href="{{ url('panel/social') }}"> Social Details </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><i class="fa fa-truck"></i> <span> Shipment Management
                            </span>
                        </a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/new_shipment') }}">Add New Shipment</a></li>
                            <li><a href="{{ url('panel/undelivered_shipment') }}">undelivered Shipment</a></li>
                            <li><a href="{{ url('panel/delivered_shipment') }}">delivered Shipment</a></li>
                            <li><a href="{{ url('panel/csv-import') }}"> import csv Shipment</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect">
                            <i class="fa fa-level-up"></i>
                            <span> Pickup management </span>

                        </a>
                        <ul class="list-unstyled">
                            <li>
                                <a href="{{ url('panel/pickup/generate') }}">Generate Pickup</a>
                            </li>
                            <li>
                                <a href="{{ url('panel/pickup/runningList') }}">Running Pickup list</a>
                            </li>
                            <li>
                                <a href="{{ url('panel/pickup/completedList') }}">Completed Pickup list</a>
                            </li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><i class="fa fa-upload"></i> <span> Manifest Management </span>
                        </a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/manifest/new') }}">Add New Manifest</a></li>
                            <li><a href="{{ url('panel/manifest/pending') }}">Pending Manifest</a></li>
                            <li><a href="{{ url('panel/manifest/received') }}">Received Manifest</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><i class="fa fa-download"></i><span> Delivery Run Sheet
                            </span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/drs/add') }}">Add DRS</a></li>
                            <li><a href="{{ url('panel/drs/completed') }}">Completed Drs</a></li>
                            <li><a href="{{ url('panel/drs/running') }}">Running Drs</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><i class=" fa fa-home "></i><span> COD </span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/cod/delivered') }}"> Delivered COD </a></li>
                            <li><a href="{{ url('panel/cod/undelivered') }}"> UnDelivered COD </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><i class="fa fa-desktop"></i><span> Global Received </span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/global_received/pickup') }}"> Pickup </a></li>
                            <li><a href="{{ url('panel/global_received/manifest') }}"> Manifest </a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{ url('panel/booking_shipment_list/all') }}" class="waves-effect"><i class="fa
                        fa-list"></i><span>  Booking Shipment
                                List
                            </span></a>
                    </li>


                    <li class="has_sub">
                        <a href="#" class="waves-effect"><i class="fa fa-road"></i><span> Routes Management </span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/route/add') }}">add Route </a></li>
                            <li><a href="{{ url('panel/route/show') }}">show route</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><i class="fa fa-users"></i><span> users Management  </span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/users-management/add_agent') }}">add agent</a></li>
                            <li><a href="{{ url('panel/users-management/add_hub') }}">add hub</a></li>
                            <li><a href="{{ url('panel/users-management/show_agents') }}"> show all agents </a></li>
                            <li><a href="{{ url('panel/users-management/show_hubs') }}"> show all hubs </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><i class="fa fa-user"></i><span> staff management </span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/staff/add') }}"> add staff</a></li>
                            <li><a href="{{ url('panel/staff/all') }}"> show staff </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><i class="fa fa-users"></i><span> Customer Management
                            </span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/customer/add') }}"> Add Customer </a></li>
                            <li><a href="{{ url('panel/customer/show') }}"> Show Customer </a></li>

                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i
                                    class="ti-share"></i><span> Messengers </span></a>
                        <ul>
                            <li class="has_sub">

                            <li><a href="{{ url('panel/messenger/add') }}"><span> Add messenger </span></a></li>
                            <li><a href="{{ url('panel/messenger/all') }}"><span> Show messenger
                                            </span></a></li>
                            <li><a href="{{ url('panel/messenger/monitor') }}"><span> monitor messenger
                                            </span></a></li>

                            </li>

                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect"><i class="fa fa-chain"></i><span> Branch Management
                            </span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/branch/add') }}"> Add new Branch </a></li>
                            <li><a href="{{ url('panel/branch/show') }}"> Show branch </a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="#" class="waves-effect "><i class="fa fa-map-marker"></i> <span> location management
                            </span>
                        </a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('panel/location/add_country') }}">add Country</a></li>
                            <li><a href="{{ url('panel/location/add_state') }}"> Add State </a></li>
                            <li><a href="{{ url('panel/location/add_city') }}"> Add city </a></li>
                            <li><a href="{{ url('panel/location/add_pincode') }}"> Add pincode </a></li>
                            <li><a href="{{ url('panel/location/pincode/list') }}"> pincode List </a></li>
                            <li><a href="{{ url('panel/location/list') }}"> Location List </a></li>
                        </ul>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->


    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <!-- ============================================================== -->
                <!--  content goes  here -->
                <!-- ============================================================== -->

                @yield('content')

                        <!-- ============================================================== -->
                <!--  content goes  here -->
                <!-- ============================================================== -->


            </div> <!-- container -->

        </div> <!-- content -->

        <!-- ============================================================== -->
        <!--  content ends  here -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <!--  start footer -->
        <!-- ============================================================== -->
        <footer class="footer text-right">
            2016 © MultiMega.
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->
    <div class="side-bar right-bar nicescroll">
        <h4 class="text-center">Chat</h4>
        <div class="contact-list nicescroll">
            <ul class="list-group contacts-list">
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="{{ asset('assets/panel/images/users/avatar-1.jpg') }}" alt="">
                        </div>
                        <span class="name">Chadengle</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="{{ asset('assets/panel/images/users/avatar-2.jpg') }}" alt="">
                        </div>
                        <span class="name">Tomaslau</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="{{ asset('assets/images/users/avatar-3.jpg') }}" alt="">
                        </div>
                        <span class="name">Stillnotdavid</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="{{ asset('assets/panel/images/users/avatar-4.jpg') }}" alt="">
                        </div>
                        <span class="name">Kurafire</span>
                        <i class="fa fa-circle online"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="{{ asset('assets/panel/images/users/avatar-5.jpg') }}" alt="">
                        </div>
                        <span class="name">Shahedk</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="{{ asset('assets/panel/images/users/avatar-6.jpg') }}" alt="">
                        </div>
                        <span class="name">Adhamdannaway</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="{{ asset('assets/panel/images/users/avatar-7.jpg') }}" alt="">
                        </div>
                        <span class="name">Ok</span>
                        <i class="fa fa-circle away"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="{{ asset('assets/panel/images/users/avatar-8.jpg') }}" alt="">
                        </div>
                        <span class="name">Arashasghari</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="{{ asset('assets/panel/images/users/avatar-9.jpg') }}" alt="">
                        </div>
                        <span class="name">Joshaustin</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
                <li class="list-group-item">
                    <a href="#">
                        <div class="avatar">
                            <img src="{{ asset('assets/panel/images/users/avatar-10.jpg') }}" alt="">
                        </div>
                        <span class="name">Sortino</span>
                        <i class="fa fa-circle offline"></i>
                    </a>
                    <span class="clearfix"></span>
                </li>
            </ul>
        </div>
    </div>
    <!-- /Right-bar -->

</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{ asset('assets/panel/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/panel/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/panel/js/detect.js') }}"></script>
<script src="{{ asset('assets/panel/js/fastclick.js') }}"></script>
<script src="{{ asset('assets/panel/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/panel/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('assets/panel/js/waves.js') }}"></script>
<script src="{{ asset('assets/panel/js/wow.min.js') }}"></script>
<script src="{{ asset('assets/panel/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('assets/panel/js/jquery.scrollTo.min.js') }}"></script>
<!-- jQuery  -->
<script src="{{ asset('assets/panel/plugins/moment/moment.js') }}"></script>


<script src="{{ asset('assets/panel/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('assets/panel/plugins/raphael/raphael-min.js') }}"></script>

<script src="{{ asset('assets/panel/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>

<!-- Todojs  -->
<script src="{{ asset('assets/panel/pages/jquery.todo.js') }}"></script>

<!-- chatjs  -->
<script src="{{ asset('assets/panel/pages/jquery.chat.js') }}"></script>

<script src="{{ asset('assets/panel/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('assets/panel/pages/jquery.dashboard_2.js') }}"></script>
<script src="{{ asset('assets/panel/pages/jquery.bs-table.js') }}"></script>
<script src="{{ asset('assets/panel/plugins/bootstrap-table/dist/bootstrap-table.min.js') }}"></script>
<script src="{{ asset('assets/panel/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/panel/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/panel/js/jquery.core.js') }}"></script>
<script src="{{ asset('assets/panel/js/jquery.app.js') }}"></script>
<script src="{{ asset('assets/panel/js/my_custom.js') }}"></script>

@yield('scripts')


</body>
</html>