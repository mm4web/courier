@extends('panel.template')
@section('content')


    <div>
        <div class="portlet">
            <div class="portlet-heading portlet-default">
                <h3 class="portlet-title text-dark">
                    Add Manifest
                </h3>
                <div class="portlet-widgets">
                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                                class="ion-minus-round"></i></a>
                    <span class="divider"></span>
                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="bg-default" class="panel-collapse collapse in">
                <div class="portlet-body">
                    {!! Form::open() !!}
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label for="source_country"> From </label>
                            <select name="source_country" id="source_country" class="form-control">
                                <option value="" selected="selected">Please Select</option>
                                <option value="1"> country1</option>
                                <option value="2">country2</option>
                                <option value="3">country3</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="messenger"> City
                            </label>
                            <select name="source_source_country" id="source_city" class="form-control">
                                <option value=""> City</option>
                                <option value=""> City2</option>
                                <option value=""> City3</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label for="destination_country"> To </label>
                            <select name="destination_country" id="destination_country" class="form-control">
                                <option value="" selected="selected">Please Select</option>
                                <option value="1"> country1</option>
                                <option value="2">country2</option>
                                <option value="3">country3</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="destination_city"> City </label>
                            <select name="destination_city" id="destination_city" class="form-control">
                                <option value=""> City</option>
                                <option value=""> City2</option>
                                <option value=""> City3</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="form-group col-xs-6">
                            {!! Form::submit('View Shipment',['class'=>'btn btn-primary']) !!}
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="date"> Date: </label>
                            {!! Form::input('date','date',null,['class'=>'form-control']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}

                    <table class="table table-striped table-bordered table-hover">
                        <colgroup>
                            <col class="con0">
                            <col class="con1">
                            <col class="con0">
                            <col class="con1">
                            <col class="con0">
                        </colgroup>

                        <thead>
                        <tr>
                            <th class="head1">Sr.No.</th>
                            <th class="head1">Select</th>
                            <th class="head0">Awb No.</th>
                            <th class="head1">Shipper</th>
                            <th class="head1">Consignee</th>
                            <!-- <th class="head1">Contents</th>-->
                            <th class="head0">Pieces</th>
                            <th class="head1">Weight</th>
                            <th class="head0">Origin</th>
                            <th class="head0">Destination</th>
                            <th class="head0">
                                Service

                                <button type="submit" name="sort_type" value="sort_service_D_S">
                                    <img src="http://courierv6.couriersoftwares.com/images/admin/sort_both.png">
                                </button>


                            </th>
                            <!--<th class="head0">Value</th>-->
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td colspan="12" align="center">Record Not Found</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop