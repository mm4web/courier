@extends('panel.template')
@section('content')

    <div>
        <div class="portlet">

            <div id="bg-default" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b> Display Manifest</b></h4>
                        <p class="text-muted font-13">
                        </p>
                        <table id="demo-custom-toolbar" data-toggle="table"
                               data-toolbar="#demo-delete-row"
                               data-search="true"
                               data-show-refresh="true"
                               data-show-toggle="true"
                               data-show-columns="true"
                               data-sort-name="id"
                               data-page-list="[5, 10, 20]"
                               data-page-size="5"
                               data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                            <thead>
                            <tr>
                                <th data-field="id" data-sortable="true"> Sr.No</th>
                                <th data-field="uniqueId" data-sortable="true">UniqueId</th>
                                <th data-field="source" data-sortable="true">Source</th>
                                <th data-field="destination" data-sortable="true">Destination</th>
                                <th data-field="date" data-sortable="true" data-formatter="dateFormatter"> date</th>
                                <th data-field="BarCode" data-sortable="true">Bar Code Number</th>
                                <th data-field="status" data-align="center" data-sortable="true"
                                    data-formatter="statusFormatter">Status
                                </th>
                                <th data-field="action" data-sortable="true" data-formatter="dateFormatter">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>3Z4A9A</td>
                                <td> Mansoura</td>
                                <td> alex</td>
                                <td>22 Jun 2015</td>
                                <td> 151548</td>
                                <td><span class="label label-success">Proceed For Pickup 0 / 1</span></td>
                                <td class="action">
                                    <a href="#" class=" btn-circle btn btn-sm btn-primary"> <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="#" class=" btn-circle btn btn-sm btn-danger"> <i class="fa fa-trash"></i>
                                    </a>
                                    <a href="#" class=" btn-circle btn btn-sm btn-info"> <i class="fa fa-print"></i>
                                    </a>
                                    <a href="#" class=" btn-circle btn btn-sm btn-warning"> <i
                                                class="fa fa-barcode"></i> </a>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop