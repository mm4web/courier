@extends('panel.template')
@section('content')

    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Add Agent
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">
                @if($errors->any())
                    @include('errors.list')
                @endif
                @if(Session::has('update_staff'))
                    <div class="alert alert-success"> {{ session('update_staff') }} </div>
                @endif
                {!! Form::model($staff) !!}

                <div class="form-group">
                    {!! Form::label('name','agent name') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('address','Address') !!}
                    {!! Form::text('address',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('phone','phone') !!}
                    {!! Form::input('number','phone',null,['class'=>'form-control']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('username','username:') !!}
                    {!! Form::text('username',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('password','password') !!}
                    <input type="password" name="password" id="password" class="form-control">
                </div>

                <div class="form-group">
                    {!! Form::label('confirmation_password','password confirmation') !!}
                    <input type="password" name="password_confirmation" id="password_conformation" class="form-control">
                </div>

                {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}

                {!! form::close() !!}
            </div>
        </div>
    </div>

@stop