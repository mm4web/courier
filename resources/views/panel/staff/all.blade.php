@extends('panel.template')
@section('content')
    <style>
        .edit {
            float: left;
            margin-right: 15px;
        }
    </style>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>show staff</b></h4>
                @if(Session::has('delete_staff'))
                    <div class="alert alert-success"> {{ session('delete_staff') }} </div>
                @endif
                <table data-toggle="table"
                       data-show-columns="false"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-switchable="false"> sr no</th>
                        <th data-field="name"> Name</th>
                        <th data-field="date">phone</th>
                        <th data-field="type" class="text-center">user type</th>
                        <th data-field="username">username</th>
                        <th data-field="action">action</th>

                    </tr>
                    </thead>

                    <tbody>
                    @foreach($staffs as $count => $staff)
                        <tr>
                            <td>{{ $count  }}</td>
                            <td> {{ $staff->name }} </td>
                            <td> {{ $staff->phone }} </td>
                            <td>staff</td>
                            <td> {{ $staff->username }} </td>
                            <td>
                                <a href="{{ url('panel/staff/edit/'.$staff->id) }}" class="edit btn btn-primary
                                btn-xs"> <i
                                            class="fa fa-pencil-square-o"></i> </a>
                                {!! Form::open(['url'=>url('panel/staff/delete/'.$staff->id)]) !!}
                                <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i>
                                </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop