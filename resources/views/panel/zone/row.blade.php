<tr>
    <td>{{ $zone->id }}</td>
    <td> {{ $zone->name }} </td>
    <td>
        <a href="{{ url('panel/zone/edit'.$zone->id) }}" class="btn btn-sm btn-primary edit"> <i class="fa
        fa-pencil-square"></i> </a>
        {!! Form::open(['id'=>'delete','url'=>url('panel/zones/delete/'.$zone->id)]) !!}
        <button class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></button>
        {!! Form::close() !!}
    </td>
</tr>