@extends('panel.template')
@section('content')
    <style>
        .edit {
            float: left;
            margin-right: 10px;
        }
    </style>
    <div class="">
        <div class="portlet">
            <div class="portlet-heading portlet-default">
                <h3 class="portlet-title text-dark">
                    Default Heading
                </h3>
                <div class="portlet-widgets">
                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                                class="ion-minus-round"></i></a>
                    <span class="divider"></span>
                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="bg-default" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="alert">
                        @if($errors->any())
                            @include('errors.list')
                        @endif
                    </div>
                    {!! Form::open(['id'=>'new_zone']) !!}
                    <div class="form-group">
                        {!! Form::label('name','Zone Name:') !!}
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'new zone']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Add Zone',['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true">SR ID</th>
                        <th data-field="date" data-sortable="true"> name</th>
                        <th data-field="action" data-sortable="true" data-formatter="dateFormatter">Action</th>
                    </tr>
                    </thead>

                    <tbody class="tbody">
                    @foreach($zones as $zone)
                        <tr>
                            <td>{{ $zone->id }}</td>
                            <td> {{ $zone->name }} </td>
                            <td>
                                <a href="{{ url('panel/zone/edit'.$zone->id) }}" class=" edit btn btn-sm
                                btn-primary"> <i
                                            class="fa
        fa-pencil-square"></i> </a>
                                {!! Form::open(['id'=>'delete','url'=>url('panel/zones/delete/'.$zone->id)]) !!}
                                {!! Form::hidden('id',$zone->id) !!}
                                <button class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop



@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            // ajax submitting new zone
            $('#new_zone').on('submit', function (e) {
                var url = $(this).attr('action');
                var name = $('input[name="name"]').val();
                var _token = $('input[name="_token"]').val();
                var data = new FormData(this);
                data.append('_token', _token);
                data.append('name', name);
                $.ajax({
                    url: url,
                    data: data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    type: 'post',
                    success: function (data) {
                        $('.alert').empty().append('<div class="alert-success alert"> zone Added successfully </div>');
                        $('.tbody').append(data.view);
                    },
                    error: function (data) {
                        var response = JSON.parse(data.responseText);
                        var errorString = '<ul>';
                        $.each(response.errors, function (key, value) {
                            errorString += '<li class="alert alert-danger">' + value + '</li>';
                        });
                        errorString += '</ul>';

                        $('.alert').empty().append(errorString);
                    }

                })
                e.preventDefault();
            });

            // ajax delete zone
            $('#delete').on('submit', function (event) {
                event.preventDefault();
                var row = $(this).parents('tr').addClass('test');
                var url = $(this).attr('action');
                var _token = $('input[name="_token"]').val();
                var data = new FormData(this);
                data.append('_token', _token);
                $.ajax({
                    url: url,
                    data: data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function () {
                        $('.alert').append('<div class="alert alert-danger"> zone deleted successfully </div>');
                        row.remove();
                    }
                });
            })
        })
    </script>
@stop