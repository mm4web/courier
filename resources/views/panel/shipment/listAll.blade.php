@extends('panel.template')
@section('content')
    <style type="text/css">
        .action a {
            border-radius: 50%;
            margin-bottom: 5px;
        }

    </style>
    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Default Heading
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">

                {!! Form::open() !!}
                <div class="row">
                    <div class="form-group col-xs-12 col-md-4 ">
                        <select name="payStatus" id="payStatus" class="form-control">
                            <option value=""> All</option>
                            <option value=""> paid</option>
                            <option value=""> pending</option>
                            <option value=""> payment at pickup</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        {!! Form::text('awb',null,['class'=>'form-control','placeholder'=>'search wb number']) !!}
                    </div>

                    <div class="form-group col-xs-12 col-md-4">
                        {!! Form::text('bookingid',null,['class'=>'form-control','placeholder'=>'search Booking Id'])
                         !!}
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true" data-formatter="invoiceFormatter">Order ID</th>
                        <th data-field="date" data-sortable="true" data-formatter="dateFormatter">Order Date</th>
                        <th data-field="awb" data-sortable="true" data-formatter="dateFormatter">AWB</th>
                        <th data-field="origin" data-sortable="true" data-formatter="dateFormatter">Origin</th>
                        <th data-field="destination" data-align="center" data-sortable="true" data-sorter="destination">
                            destination
                        </th>
                        <th data-field="sender" data-sortable="true" data-formatter="dateFormatter">Sender</th>
                        <th data-field="status" data-align="center" data-sortable="true"
                            data-formatter="statusFormatter">Status
                        </th>
                        <th data-field="action" data-sortable="true" data-formatter="dateFormatter">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>UB-1609</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td> mansoura</td>
                        <td> Ali</td>
                        <td>Unpaid</td>
                        <td class="action">
                            <a href="#" class=" btn-circle btn btn-sm btn-primary"> <i class="fa fa-exclamation-circle fa-fw"></i> </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-danger"> <i class="fa fa-trash"></i> </a>
                        </td>
                    </tr>
                    <tr>

                        <td>UB-1610</td>
                        <td>24 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Paid</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1611</td>
                        <td>25 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Shipped</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1612</td>
                        <td>27 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Refunded</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1613</td>
                        <td>28 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Unpaid</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>UB-1614</td>
                        <td>29 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Paid</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1615</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Shipped</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1616</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Refunded</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1617</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Unpaid</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>UB-1618</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Paid</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1619</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Shipped</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1620</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Refunded</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1621</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Unpaid</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>UB-1622</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Paid</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1623</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Shipped</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>UB-1624</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name</td>
                        <td></td>
                        <td></td>
                        <td>Refunded</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').dataTable();
        });
    </script>
@stop