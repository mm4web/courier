@extends('panel.template')

@section('content')

    <style type="text/css">
        .action a {
            border-radius: 50%;
            width: 35px;
            height: 35px;
            margin-bottom: 5px;
        }

    </style>
    <!--Custom Toolbar-->
    <!--===================================================-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b> undelivered Shipments </b></h4>
                <p class="text-muted font-13">
                </p>

                <button id="demo-delete-row" class="btn btn-danger" disabled><i class="fa fa-times m-r-5"></i>Delete
                </button>
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="state" data-checkbox="true"></th>
                        <th data-field="id" data-sortable="true" data-formatter="invoiceFormatter">Order ID</th>
                        <th data-field="date" data-sortable="true" data-formatter="dateFormatter">Order Date</th>
                        <th data-field="awb" data-sortable="true" data-formatter="dateFormatter">AWB</th>
                        <th data-field="origin" data-sortable="true" data-formatter="dateFormatter">Origin</th>
                        <th data-field="destination" data-align="center" data-sortable="true" data-sorter="destination">
                            destination
                        </th>
                        <th data-field="sender" data-sortable="true" data-formatter="dateFormatter">Sender</th>
                        <th data-field="status" data-align="center" data-sortable="true"
                            data-formatter="statusFormatter">Status
                        </th>
                        <th data-field="action" data-sortable="true" data-formatter="dateFormatter">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td></td>
                        <td>UB-1609</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td> mansoura </td>
                        <td> Ali </td>
                        <td>Unpaid</td>
                        <td class="action">
                            <a href="#" class=" btn-circle btn btn-sm btn-primary"> <i class="fa fa-edit"></i> </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-danger"> <i class="fa fa-trash"></i> </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-info"> <i class="fa fa-print"></i> </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-warning"> <i class="fa fa-barcode"></i> </a>
                            <br>
                            <a href="#" class=" btn-circle btn btn-sm btn-success"> <i class="fa fa-file-text-o"></i>
                            </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-success"> <i class="fa  fa-file-pdf-o"></i>
                            </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-success"> <i class="fa fa-repeat"></i> </a>
                            <a href="#" class=" btn-circle btn btn-sm btn-success"> <i class="fa fa-share "></i> </a>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>UB-1610</td>
                        <td>24 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Paid</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1611</td>
                        <td>25 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Shipped</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1612</td>
                        <td>27 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Refunded</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1613</td>
                        <td>28 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Unpaid</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>UB-1614</td>
                        <td>29 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Paid</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1615</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Shipped</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1616</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Refunded</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1617</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Unpaid</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>UB-1618</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Paid</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1619</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Shipped</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1620</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Refunded</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1621</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Unpaid</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>UB-1622</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Paid</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1623</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Shipped</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>UB-1624</td>
                        <td>22 Jun 2015</td>

                        <td>123</td>
                        <td> shipment name </td>
                        <td></td>
                        <td></td>
                        <td>Refunded</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        var resizefunc = [];
    </script>
@stop