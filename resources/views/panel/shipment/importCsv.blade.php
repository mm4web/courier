@extends('panel.template')
@section('content')
    <style>
        .alert a {
            float: right;
        }

        table {
            margin-bottom: 50px !important;
        }

        label {
            display: block;
        }

    </style>
    <h2> Impost CSV Shipment</h2>
    <div class="alert alert-info">
        <strong>Heads up!</strong> This alert needs your attention, but it's not super important.
        <a href="#"> <i class="fa fa-file-pdf-o "></i> Download A simple excel CSV here </a>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Panel Default</h3>
        </div>
        <div class="panel-body">
            <div class="p-20">
                <table class="table table-striped table-bordered table-hover m-0">
                    <tbody>
                    <tr>
                        <td>(1) Weight(KG)</td>
                        <td>(2) Product/Type</td>
                        <td>(3) Booking Mode</td>
                    </tr>
                    <tr>
                        <td>(4) Mode</td>
                        <td>(5) Origin</td>
                        <td>(6) Destination</td>
                    </tr>
                    <tr>
                        <td>(7) Pieces</td>
                        <td>(8) Volumetric weight(KG)</td>
                        <td>(9) Pickup Time(24HR)</td>
                    </tr>
                    <tr>
                        <td>(10) Pickup Date</td>
                        <td>(11) Status</td>
                        <td>(12) Expected Delivery Date</td>
                    </tr>
                    <tr>
                        <td>(13) Shipper's Account no.</td>
                        <td>(14)Shipper's Reference no.</td>
                        <td>(15)Shipper's Name</td>
                    </tr>
                    <tr>
                        <td>(16)Shipper's Code</td>
                        <td>(17)Shipper's Mobile</td>
                        <td>(18)Shipper's Fax</td>
                    </tr>
                    <tr>
                        <td>(19)Shipper's E-mail</td>
                        <td>(20)Shipper's Address</td>
                        <td>(21)Consignor Name</td>
                    </tr>
                    <tr>
                        <td>(22)Consignor Address</td>
                        <td>(23)Consignor Zip Code</td>
                        <td>(24)Consignor Mobile</td>
                    </tr>
                    <tr>
                        <td>(25)Consignor Fax</td>
                        <td>(26)Consignor E-mail</td>
                        <td>(27)Description Service Charge</td>
                    </tr>
                    <tr>
                        <td>(28)Description Packing Charge</td>
                        <td>(29)Description Valuation Charges</td>
                        <td>(30)Description Other Charges</td>
                    </tr>
                    <tr>
                        <td>(31)Description Services Tax</td>
                        <td>(32)Messenger Courier Messenger</td>
                        <td>(33)Messenger Route Code</td>
                    </tr>
                    <tr>
                        <td>(34)Messenger Declared Value</td>
                        <td>(35)COD Amount</td>
                        <td></td>
                    </tr>

                    </tbody>
                </table>
                <br><br>
                {!! Form::open() !!}
                <label for=""> import file to upload </label>
                <div class="fileupload btn btn-purple waves-effect waves-light">
                    <span><i class="ion-upload m-r-5"></i>Choose File</span>
                    <input type="file" class="upload">
                </div>
                <br>
                <br>
                {!! Form::submit('submit Shipment',['class'=>'btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop