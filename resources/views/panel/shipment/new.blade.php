@extends('panel.template')

@section('content')
    <style type="text/css">
        .portlet-body {
            background: #ebeff2 !important;
            overflow: hidden;
        }

        label {
            margin-right: 10px;
        }

        thead th {
            text-align: center !important;
        }
    </style>
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Company Details</h4>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Admin</a>
                </li>
                <li>
                    <a href="#">Shipment Management </a>
                </li>
                <li class="active">
                    New Shipment
                </li>
            </ol>
        </div>

        @if($errors->any())
            @include('errors.list')
        @endif
        {{--
        *************************************->
       facebook Settings
        **********************************->
        --}}

        <div class="col-xs-12">
            <div class="portlet">
                <div class="portlet-heading portlet-default">
                    <h3 class="portlet-title text-dark">
                        New Shipment
                    </h3>
                    <div class="portlet-widgets">
                        <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                        <span class="divider"></span>
                        <a data-toggle="collapse" data-parent="#accordion1" href="#facebook"><i
                                    class="ion-minus-round"></i></a>
                        <span class="divider"></span>
                        <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="facebook" class="panel-collapse collapse in">
                    <div class="portlet-body">
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Date & Time </h3>
                                </div>
                                <div class="panel-body">
                                    {!! Form::open() !!}
                                    <div class="form-group col-xs-12 col-md-6">
                                        {!! Form::label('date','Date:') !!}
                                        {!! Form::input('date','date',date('Y-m-d'),['class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group col-xs-12 col-md-6">
                                        {!! Form::label('Time','Time:') !!}
                                        {!! Form::input('time','time',null,['class'=>'form-control']) !!}
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Shipment Type </h3>
                                </div>
                                <div class="panel-body">

                                    <div class="form-group col-xs-12 col-md-6">
                                        Product/Type:
                                        <br>
                                        <label><input type="radio" name="f_available" checked value="1">Parcel</label>
                                        <label><input type="radio" name="f_available" value="0">Pallate</label>
                                    </div>
                                    <div class="form-group col-xs-12 col-md-6">
                                        {!! Form::label('payment','payment Mode:') !!}
                                        <select name="payment" id="payment" class="form-control">
                                            <option value="CREDIT">CREDIT</option>
                                            <option value="CASH">Paid</option>
                                            <option value="ToPay">ToPay</option>
                                            <option value="COD">Cash on Delivery</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Location Info </h3>
                                </div>
                                <div class="panel-body">

                                    <div class="form-group col-xs-12 col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"> Sender Info </h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    {!! Form::label('sender_code','Agent Code') !!}
                                                    {!! Form::text('sender_code',null,['class'=>'form-control',
                                                    'id'=>'sender_code','data-link'=>url
                                                    ('panel/new_shipment/agent_code')])
                                                     !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('s_name','Name*') !!}
                                                    {!! Form::text('s_name',null,['class'=>'form-control',
                                                    'placeholder'=>'name']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('s_country','Country* ') !!}
                                                    <select class="form-control" name="s_country" id="country"
                                                            data-link="{{ url('panel/locations/getCity') }}">
                                                        <option value=""> --select-- </option>
                                                        @foreach($countries as $country)
                                                            <option value="{{ $country->id }}"> {{ $country->country }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('s_city','City*') !!}
                                                    <select class="form-control" name="s_city" id="cities">
                                                        <option value=""> --select--</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('s_zip','Zip Code*') !!}
                                                    {!! Form::input('number','s_zip',null,['class'=>'form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('s_address','Address*') !!}
                                                    {!! Form::textarea('s_address',null,['class'=>'form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('s_phone','Phone*') !!}
                                                    {!! Form::input('number','s_phone',null,['class'=>'form-control'])
                                                     !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('s_email','Email*') !!}
                                                    {!! Form::email('s_email',null,['class'=>'form-control']) !!}
                                                </div>
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="" name="s_save_addr"> Save
                                                        Address </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"> Receiver Info </h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    {!! Form::label('receiver_code','Agent Code') !!}
                                                    {!! Form::text('receiver_code',null,['class'=>'form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('r_name','Name*') !!}
                                                    {!! Form::text('r_name',null,['class'=>'form-control',
                                                    'placeholder'=>'name']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('r_country','Country* ') !!}
                                                    <select class="form-control" name="r_country" id="country">
                                                        <option value="eg"> Egypt</option>
                                                        <option value="us"> United State</option>
                                                        <option value="en"> England</option>
                                                        <option value="fr"> France</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('r_city','City*') !!}
                                                    <select class="form-control" name="r_city" id="country">
                                                        <option value="eg"> mansoura</option>
                                                        <option value="us"> cairo</option>
                                                        <option value="en"> alex</option>
                                                        <option value="fr"> giza</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('r_zip','Zip Code*') !!}
                                                    {!! Form::input('number','r_zip',null,['class'=>'form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('r_address','Address*') !!}
                                                    {!! Form::textarea('r_address',null,['class'=>'form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('r_phone','Phone*') !!}
                                                    {!! Form::input('number','r_phone',null,['class'=>'form-control'])
                                                     !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('r_email','Email*') !!}
                                                    {!! Form::email('r_email',null,['class'=>'form-control']) !!}
                                                </div>
                                                <div class="checkbox">
                                                    <label><input type="checkbox" value="" name="r_save_addr"> Save
                                                        Address </label>
                                                </div>
                                            </div>
                                        </div>
                                        Bill To:
                                        <label><input type="radio" name="bill_to" checked value="s">sender</label>
                                        <label><input type="radio" name="bill_to" value="r">Receiver</label>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Shipment Info </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-6 ">
                                                <label for="ship_status"> Status: </label>
                                                <select name="ship_status" class="form-control" id="ship_status">
                                                    <option value="B">Booked</option>
                                                    <option value="T">Picked&nbsp;up</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-6">
                                                {!! Form::label('no_parcel','No. of Parcel:') !!}
                                                {!! Form::input('number','parcel_number','1',['class'=>'form-control',
                                                'id'=>'parcel'])
                                                 !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>sr.no</th>
                                                <th>diamentions</th>
                                                <th>weight</th>
                                            </tr>
                                            </thead>
                                            <tbody class="tbody">
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <div class="col-xs-4">
                                                        <div class="form-group">
                                                            <input type="number" class="form-control"
                                                                   name="parcel_length_1"
                                                                   placeholder="length">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="form-group">
                                                            <input type="number" class="form-control"
                                                                   name="parcel_width_1"
                                                                   placeholder="width">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div class="form-group">
                                                            <input type="number" class="form-control"
                                                                   name="parcel_height_1"
                                                                   placeholder="height">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="number" name="weight" id="weight"
                                                               class="form-control" placeholder="weight">
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Service & Payment Info </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-12 col-md-6">
                                        <h3> Service </h3>
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td class="col-xs-3">
                                                    service (Mode)*
                                                </td>
                                                <td class="col-xs-9">
                                                    <div class="form-group">
                                                        <select name="service_id" id="" class="form-control">
                                                            <option value=""> Select service</option>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-xs-3">
                                                    Shipment Value
                                                </td>
                                                <td class="col-xs-9">
                                                    <div class="form-group">
                                                        {!! Form::text('shipment_value',null,['class'=>'form-control']) !!}
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-xs-3">
                                                    Description
                                                </td>
                                                <td class="col-xs-9">
                                                    <div class="form-group">
                                                        {!! Form::textarea('status_description',null,
                                                        ['class'=>'form-control']) !!}
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <h3> Payment Details </h3>
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr class="row">
                                                <td class="col-xs-4">
                                                    <strong>
                                                        Chargeable Weight(Kg)
                                                    </strong>
                                                </td>
                                                <td class="col-xs-8">
                                                    {!! Form::text('chargeable_weight',null,['class'=>'form-control',
                                                    'readonly'=>true])
                                                     !!}
                                                </td>
                                            </tr>
                                            <tr class="row">
                                                <td class="col-xs-4">
                                                    <strong>
                                                        Total Weight(Kg)
                                                    </strong>
                                                </td>
                                                <td class="col-xs-8">
                                                    {!! Form::text('total_weight',null,['class'=>'form-control',
                                                    'readonly'=>true])
                                                     !!}
                                                </td>
                                            </tr>
                                            <tr class="row">
                                                <td class="col-xs-4">
                                                    <strong>
                                                        delivery charge
                                                    </strong>
                                                </td>
                                                <td class="col-xs-8">
                                                    {!! Form::text('delivery_charge',null,['class'=>'form-control'])
                                                     !!}
                                                </td>
                                            </tr>
                                            <tr class="row">
                                                <td class="col-xs-4">
                                                    <strong>
                                                        Services Tax (14%)$
                                                    </strong>
                                                </td>
                                                <td class="col-xs-8">
                                                    {!! Form::text('service_tax',null,['class'=>'form-control',
                                                    'readonly'=>true])
                                                     !!}
                                                </td>
                                            </tr>
                                            <tr class="row">
                                                <td class="col-xs-4">
                                                    <strong>
                                                        Total charge $
                                                    </strong>
                                                </td>
                                                <td class="col-xs-8">
                                                    {!! Form::text('total_charge',null,['class'=>'form-control',
                                                    'readonly'=>true])
                                                     !!}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('scripts')
    <script type="text/javascript">

        $('#parcel').change(function () {
            $('.tbody').empty();
            for (var i = 1; i <= $(this).val(); i++) {
                $('.tbody').append('<tr>' + '<td>' + i + '</td>' + '<td><div class="col-xs-4">' +
                        '<div class="form-group">' +
                        '<input type="number" class="form-control"' +
                        'name="parcel_length_' + i + '"' +
                        'placeholder="length">' +
                        '</div></div>' +
                        '<div class="col-xs-4">' +
                        '<div class="form-group">' +
                        '<input type="number" class="form-control"' +
                        'name="parcel_width_' + i + '"' +
                        'placeholder="width">' + '</div></div>' +
                        '<div class="col-xs-4">' +
                        '<div class="form-group">' +
                        '<input type="number" class="form-control"' +
                        'name="parcel_height_' + i + '"' +
                        'placeholder="height">' +
                        '</div></div>' +
                        '</td>' +
                        '<td>' +
                        '<div class="form-group">' +
                        '<input type="number" name="parcel_weight_' + i + '"' + ' id="weight"' +
                        'class="form-control" placeholder="weight">' +
                        '</div>' +
                        '</td>');
            }
        })

        $('#sender_code').change(function () {
            var agent_code = $(this).val();
            var url = $(this).data('link');
            var user_id = '';
            $.ajax({
                url: url,
                data: {"agent_code": agent_code},
                dataType: 'json',
                success: function (response) {
                    $('input[name=s_name]').val(response.user.name);
                    user_id += response.user.id;
                    $('#country').val('' + response.user.country_id + '');
                    var country_url = $('#country').data('link');
                    var country_id = $('#country').val();
                    $.ajax({
                        url: country_url,
                        data: {'country_id': country_id},
                        dataType: 'json',
                        success: function (data) {
                            $('#cities').empty().append('<option value=""> -- select state -- </option>');
                            $(data.data).each(function (cities, city) {
                                $('#cities').append('<option value="' + city.id + '">' + city.name + '</option>')
                            })
                        }
                    })
                    $('input[name=s_zip]').val(response.user.zip);
                    $('textarea[name=s_address]').val(response.user.address);
                    $('input[name=s_phone]').val(response.user.phone);
                    $('input[name=s_email]').val(response.user.email);
                }
            })
        })

    </script>
@stop
