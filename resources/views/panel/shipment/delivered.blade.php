@extends('panel.template')

@section('content')

    <style type="text/css">
        .action a {
            border-radius: 50%;
            width: 35px;
            height: 35px;
            margin-bottom: 5px;
        }

    </style>
    <!--Custom Toolbar-->
    <!--===================================================-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b> undelivered Shipments </b></h4>
                <p class="text-muted font-13">
                </p>

                <button id="demo-delete-row" class="btn btn-danger" disabled><i class="fa fa-times m-r-5"></i>Delete
                </button>
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="state" data-checkbox="true"></th>
                        <th data-field="id" data-sortable="true" data-formatter="invoiceFormatter"> sr .No</th>
                        <th data-field="date" data-sortable="true" data-formatter="dateFormatter">Order Date</th>
                        <th data-field="name" data-sortable="true">Name</th>
                        <th data-field="awb" data-sortable="true" data-formatter="dateFormatter">AWB</th>
                        <th data-field="origin" data-sortable="true" data-formatter="dateFormatter">Origin</th>
                        <th data-field="destination" data-align="center" data-sortable="true" data-sorter="destination">
                            destination
                        </th>
                        <th data-field="sender" data-sortable="true" data-formatter="dateFormatter">Sender</th>
                        <th data-field="product_type" data-sortable="true" data-formatter="dateFormatter">product_type
                        </th>
                        <th data-field="status" data-align="center" data-sortable="true"
                            data-formatter="statusFormatter">Status
                        </th>
                        <th data-field="action" data-sortable="true" data-formatter="dateFormatter">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td></td>
                        <td>UB-1609</td>
                        <td>22 Jun 2015</td>
                        <td>Boudreaux</td>
                        <td>123</td>
                        <td> shipment name</td>
                        <td> mansoura</td>
                        <td> Ali</td>
                        <td> Parcel</td>
                        <td>delivered</td>
                        <td class="action">
                            <a href="#" class=""> <i class="fa fa-barcode"></i> </a>
                            <a href="#" class=""> <i class="fa fa-print"></i> </a>
                            <a href="#" class=""> <i class="fa fa-picture-o"></i> </a>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>UB-1610</td>
                        <td>24 Jun 2015</td>
                        <td>Woldt</td>
                        <td>123</td>
                        <td> shipment name</td>
                        <td> giza</td>
                        <td> mohamed</td>
                        <td> Parcel</td>
                        <td>delivered</td>
                        <td class="action">
                            <a href="#" class=""> <i class="fa fa-barcode"></i> </a>
                            <a href="#" class=""> <i class="fa fa-print"></i> </a>
                            <a href="#" class=""> <i class="fa fa-picture-o"></i> </a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        var resizefunc = [];
    </script>
@stop