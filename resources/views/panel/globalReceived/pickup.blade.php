@extends('panel.template')
@section('content')
    <h2> Picked Up Shipment In </h2>
    <div class="portlet">
        <div class="portlet-heading bg-custom">
            <h3 class="portlet-title">
                shipment in
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-primary"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-primary" class="panel-collapse collapse in">
            <div class="portlet-body">

                <div class="alert alert-info">
                    Driver/Messenger's Picked Up shipment will "IN" here. You can use Barcode Scanner or type the AWB
                    number
                    to In the Shipment.
                </div>

                {!! Form::open() !!}
                <div class="form-group">
                    {!! Form::text('search',null,['class'=>'form-control','placeholder'=>'please enter your awb no'])
                     !!}
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop