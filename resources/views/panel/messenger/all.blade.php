@extends('panel.template')

@section('content')

    <style type="text/css">
        a.edit {
            float: left;
            margin-right: 15px;
        }

    </style>
    <!--Custom Toolbar-->
    <!--===================================================-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b> show customers </b></h4>
                @if(Session::has('delete_messenger'))
                    <div class="alert alert-success"> {{ session('delete_messenger') }} </div>
                @endif
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true">#</th>
                        <th data-field="uniqueId" data-sortable="true">UniqueId</th>
                        <th data-field="Name" data-sortable="true">Name</th>
                        <th data-field="phone" data-sortable="true">phone</th>
                        <th data-field="email" data-sortable="true">email</th>
                        <th data-field="action" data-sortable="true">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($messengers as $key => $messenger)
                        <tr>

                            <td> {{ $key }} </td>
                            <td> {{ $messenger->code }} </td>

                            <td> {{ $messenger->name }} </td>
                            <td>  {{ $messenger->mobil }} </td>
                            <td> {{ $messenger->email }} </td>
                            <td class="action">
                                <a href="{{ url('panel/messenger/edit/'.$messenger->id) }}"
                                   class=" edit btn-circle btn btn-sm btn-primary"> <i class="fa
                                fa-pencil-square-o  "></i> </a>
                                {!! Form::open(['url'=>url('panel/messenger/delete/'.$messenger->id)]) !!}
                                <button class="btn btn-sm btn-danger btn-circle" type="submit"><i class="fa
                                fa-trash"></i>
                                </button>
                                {!! Form::close() !!}

                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        var resizefunc = [];
    </script>
@stop