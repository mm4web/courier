@extends('panel.template')
@section('content')

    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Add messenger
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">

                @if($errors->any())
                    @include('errors.list')
                @endif

                @if(Session::has('new_messenger'))
                    <div class="alert alert-success">  {{ session('new_messenger') }} </div>
                @endif

                {!! Form::model($messenger,['files'=>true]) !!}

                <div class="form-group">
                    {!! Form::label('name',' name') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('code','code') !!}
                    {!! Form::input('number','code',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('country_id','country') !!}
                    <select name="country_id" data-link="{{ url('panel/messenger/getCity') }}" id="country_id"
                            class="form-control">
                        <option value=""> --select country --</option>
                        @foreach($countries as $country)
                            @if($country->id == $messenger->country_id)
                                <option value="{{ $country->id }}" selected> {{ $country->country }} </option>
                            @endif
                            <option value="{{ $country->id }}"> {{ $country->country }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    {!! Form::label('city_id','city location') !!}
                    <select name="city_id" data-link="{{ url('panel/messenger/getBranches') }}" id="cities"
                            class="form-control">
                        @foreach($cities as $city)
                            @if($city->id == $messenger->city_id)
                                <option value="{{ $city->id }}" selected> {{ $city->name }} </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('branch_id','branch') !!}
                    <select name="branch_id" data-link="{{ url('panel/messenger/getCity') }}" id="branches"
                            class="form-control">
                        @foreach($branches as $branch)
                            @if($branch->id == $messenger->branch_id)
                                <option value="{{ $branch->id }}" selected> {{ $branch->person }} </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('mobile','mobile') !!}
                    {!! Form::input('number','mobil',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email','email') !!}
                    {!! Form::email('email',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('password','password') !!}
                    <input type="password" name="password" id="password" class="form-control">
                </div>

                <div class="form-group">
                    {!! Form::label('confirmation_password','password confirmation') !!}
                    <input type="password" name="password_confirmation" id="password_conformation" class="form-control">
                </div>

                <div class="form-group">
                    {!! Form::label('vehicle_number','Vehicle Number:') !!}
                    {!! Form::input('number','vehicle_number',null,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('image','image:') !!}
                    {!! Form::file('image',null,['class'=>'form-control']) !!}
                </div>


                {!! Form::submit('add messenger ',['class'=>'btn btn-primary']) !!}

                {!! form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $('#country_id').change(function () {
            var country_id = $(this).val();
            var link = $(this).data('link');
            $.ajax({
                url: link,
                data: {'country_id': country_id},
                success: function (data) {
                    $('#cities').empty().append('<option value=""> -- select city -- </option>');
                    $(data.cities).each(function (cities, city) {
                        $('#cities').append('<option value="' + city.id + '">' + city.name + '</option>')
                    })
                }
            })
        })

        $('#cities').change(function () {
            var city_id = $(this).val();
            var link = $(this).data('link');
            $.ajax({
                url: link,
                data: {'city_id': city_id},
                success: function (data) {
                    $('#branches').empty().append('<option value=""> -- select branch -- </option>');
                    $(data.branches).each(function (branches, branch) {
                        $('#branches').append('<option value="' + branch.id + '">' + branch.person + '</option>')
                    })
                }
            })
        })
    </script>
@stop