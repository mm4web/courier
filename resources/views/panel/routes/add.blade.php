@extends('panel.location_template')
@section('content')
    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Add Route
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">
                @if($errors->any())
                    @include('errors.list')
                @endif
                @if(Session::has('new_route'))
                    <div class="alert alert-success"> {{ session('new_route') }} </div>
                @endif
                {!! Form::open() !!}
                <div class="form-group">
                    {!! Form::label('country_id','Select country') !!}
                    <select name="country_id" id="country" data-link="{{ url('panel/route/getCities') }}"
                            class="form-control">
                        <option value=""> -- select country --</option>
                        @foreach($countries as $country)
                            <option value="{{ $country->id }}">{{ $country->country }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('city_id','select City') !!}
                    <select name="city_id" id="city"
                            class="form-control">
                        <option value=""> --select city--</option>
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::label('route_code','Route Code') !!}
                    {!! Form::input('number','route_code',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('name','Route') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script type="text/javascript">
        $('#country').change(function () {
            var url = $(this).data('link');
            var country_id = $(this).val();
            $.ajax({
                url: url,
                data: {"country_id": country_id},
                success: function (data) {
                    $('#city').empty().append('<option value=""> -- select city -- </option>');
                    $(data.cities).each(function (city, value) {
                        $('#city').append('<option value="' + value.id + '">' + value.name + '</option>');
                    })
                }
            })
        })
    </script>
@stop