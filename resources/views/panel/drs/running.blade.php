@extends('panel.template')
@section('content')

    <div>
        <div class="portlet">

            <div id="bg-default" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b> All DRS (Delivery Run Sheet) </b></h4>
                        <p class="text-muted font-13">
                        </p>

                        <button id="demo-delete-row" class="btn btn-danger" disabled><i class="fa fa-times m-r-5"></i>Delete
                        </button>
                        <table id="demo-custom-toolbar" data-toggle="table"
                               data-toolbar="#demo-delete-row"
                               data-search="true"
                               data-show-refresh="true"
                               data-show-toggle="true"
                               data-show-columns="true"
                               data-sort-name="id"
                               data-page-list="[5, 10, 20]"
                               data-page-size="5"
                               data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                            <thead>
                            <tr>
                                <th data-field="id" data-sortable="true"> #</th>
                                <th data-field="uniqueId" data-sortable="true">UniqueId</th>
                                <th data-field="date" data-sortable="true" data-formatter="dateFormatter"> Date
                                </th>
                                <th data-field="city" data-sortable="true">City</th>
                                <th data-field="RouteCode" data-sortable="true">Route Code</th>
                                <th data-field="status" data-align="center" data-sortable="true"
                                    data-formatter="statusFormatter">Status
                                </th>
                                <th data-field="action" data-sortable="true" data-formatter="dateFormatter">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>3Z4A9A</td>
                                <td>22 Jun 2015</td>
                                <td>mansoura</td>
                                <td> 1</td>
                                <td><span class="label label-warning"> Running(13)	 </span></td>
                                <td class="action">
                                    <a href="#" class=" btn-circle btn btn-sm btn-success"> <i
                                                class="fa fa-exclamation-circle fa-fw"></i> </a>
                                    <a href="#" class=" btn-circle btn btn-sm btn-warning"> <i class="fa fa-print"></i>
                                    </a>
                                    <a href="#" class=" btn-circle btn btn-sm btn-success"> <i class="fa fa-repeat"></i>
                                    </a>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop