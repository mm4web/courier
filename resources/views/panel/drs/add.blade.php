@extends('panel.template')
@section('content')


    <div>
        <div class="portlet">
            <div class="portlet-heading portlet-default">
                <h3 class="portlet-title text-dark">
                    Add Delivery Run Sheet
                </h3>
                <div class="portlet-widgets">
                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                                class="ion-minus-round"></i></a>
                    <span class="divider"></span>
                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="bg-default" class="panel-collapse collapse in">
                <div class="portlet-body">
                    {!! Form::open() !!}
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label for="route_code"> Select Country </label>
                            <select name="route_code" id="route_code" class="form-control">
                                <option value="" selected="selected">Please Select</option>
                                <option value="334889">334889</option>
                                <option value="334002">334002</option>
                                <option value="334002">334002</option>
                                <option value="1">1</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="messenger"> Select City <a href="#"> <i class="fa fa-plus"></i> Add City </a>
                            </label>
                            <select name="messenger" id="messenger" class="form-control">
                                <option value=""> please select</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label for="route_code"> Route Code <a href="#"> <i class="fa fa-plus"></i> Add new route
                                </a> </label>
                            <select name="route_code" id="route_code" class="form-control">
                                <option value="" selected="selected">Please Select</option>
                                <option value="334889">334889</option>
                                <option value="334002">334002</option>
                                <option value="334002">334002</option>
                                <option value="1">1</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="messenger"> Select messenger <a href="#"> <i class="fa fa-plus"></i> Add
                                    messenger
                                </a> </label>
                            <select name="messenger" id="messenger" class="form-control">
                                <option value=""> please select</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    {!! Form::submit('create pickup list',['class'=>'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop