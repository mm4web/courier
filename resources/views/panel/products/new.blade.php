@extends('panel.location_template')
@section('content')
    <style>
        .radio input[type="radio"] {
            opacity: 1;
        }

        .radio label::before {
            border: none;
        }
    </style>
    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                New Product
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">

                @if($errors->any())
                    @include('errors.list')
                @endif

                @if(Session::has('new_product'))
                    <div class="alert alert-success">  {{ session('new_product') }} </div>
                @endif

                {!! Form::open(['files'=>true]) !!}
                <h3> Product type info </h3>
                <div class="form-group">
                    {!! Form::label('name',' Product name: ') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>
                <h3>Weight Range Setup</h3>
                <div class="form-group">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Sr.no.</th>
                            <th>Start range in</th>
                            <th>End range in</th>
                            <th>
                                <a href="#" class="btn btn-dribbble" id="new_range"> Add <i class="fa fa-plus"></i></a>
                            </th>
                        </tr>
                        <tbody class="weight_rang">
                        <tr>
                            <td>#0</td>
                            <td><input type="text" name="start_range[0]['from']" class="form-control" value="0"></td>
                            <td><input type="text" name="end_range[0]['to']" class="form-control"></td>
                            <td></td>
                        </tr>
                        </tbody>
                        </thead>
                    </table>
                </div>
                <h3>Weight Info</h3>
                <div class="form-group">
                    <input type="text" name="weight" id="weight" class="form-control">
                </div>
                <h3> dimensions Setup </h3>

                <p>Do You want to Set Dimensions Yes</p>
                <div class="radio">
                    <label>
                        <input type="radio" name="dimensions" id="optionsRadios1" value="1" checked>
                        yes
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="dimensions" id="optionsRadios2" value="0">
                        no
                    </label>
                </div>
                <div class="dimensions">
                    <div class="form-group">
                        <label for="height">Maximum Height</label>
                        <input type="text" name="height" id="height" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="width">Maximum Width</label>
                        <input type="text" name="width" id="width" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="length">Maximum Length</label>
                        <input type="text" name="length" id="length" class="form-control">
                    </div>
                </div>
                {!! Form::submit('add product ',['class'=>'btn btn-primary']) !!}

                {!! form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script type='text/javascript'>
        var counter = 1;
        $('#new_range').on('click', function (e) {
            e.preventDefault();
            counter += 1;
            $('.weight_rang').append('<tr> <td> #' + counter + '</td> <td> ' +
                    '<input type="text" name="start_range[' + counter + '][' + "from" + ']"class="form-control"> </td> ' +
                    '<td> <input type="text" name="end_range[' + counter + '][' + "to" + ']" class="form-control"> </td>' +
                    '<td></td></tr>'
            );
        })

        $('input[name=dimensions]').change(function () {
            var val = $(this).val();
            if (val == 1) {
                $('.dimensions').show(300);
            } else {
                $('.dimensions').hide(300);
            }
        })

    </script>
@stop
