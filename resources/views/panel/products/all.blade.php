@extends('panel.location_template')
@section('content')

    <style type="text/css">
        .action a {
            border-radius: 50%;
            width: 35px;
            height: 35px;
            margin-bottom: 5px;
        }

        .edit {
            float: left;
            margin-right: 10px;
        }
    </style>
    <!--Custom Toolbar-->
    <!--===================================================-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b> show branches </b></h4>
                @if(Session::has('branch_status'))
                    <p class="text-muted font-13 alert alert-success">
                        {{ session('branch_status') }}
                    </p>
                @endif
                @if($errors->any())
                    @include('errors.list')
                @endif
                @if(Session::has('delete_branch'))
                    <div class="alert alert-success"> {{ session('delete_branch') }} </div>
                @endif
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true"> sr .No</th>
                        <th data-field="type" data-sortable="true">Product / Type Name</th>
                        <th data-field="action" data-sortable="true">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($products as $count => $product)
                        <tr>
                            <td> {{ $count+1 }} </td>
                            <td> {{ $product->name }} </td>

                            <td class="action">
                                {!! Form::open(['url'=>url('panel/product/edit/'.$product->id),'class'=>'form-inline
                                edit',
                                'method'=>'get']) !!}
                                <button type="submit" class="btn btn-primary"><i class="fa
                                fa-pencil-square-o"></i></button>
                                {!! Form::close() !!}

                                {!! Form::open(['url'=>url('panel/product/delete/'.$product->id)]) !!}
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        var resizefunc = [];
    </script>
@stop