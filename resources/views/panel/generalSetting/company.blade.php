@extends('panel.template')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Company Details</h4>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Admin</a>
                </li>
                <li>
                    <a href="#">General Setting</a>
                </li>
                <li class="active">
                    company Details
                </li>
            </ol>
        </div>

        @if($errors->any())
            @include('errors.list')
        @endif
        {{--
        *************************************->
       admin login
        **********************************->
        --}}

        <div class="col-xs-12">
            <div class="portlet">
                <div class="portlet-heading portlet-default">
                    <h3 class="portlet-title text-dark">
                        <i class="fa fa-user fa-2x"></i> Admin Login Setup
                    </h3>
                    <div class="portlet-widgets">
                        <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                        <span class="divider"></span>
                        <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                                    class="ion-minus-round"></i></a>
                        <span class="divider"></span>
                        <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="bg-default" class="panel-collapse collapse in">
                    <div class="portlet-body">
                        {!! Form::open() !!}
                        <div class="form-group">
                            {!! Form::label('name','User Name :') !!}
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password','Password:') !!}
                            <input id="password" type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-purple waves-effect waves-light" type="submit"> update</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        {{--
        *************************************->
        company info
        **********************************->
        --}}


        <div class="col-xs-12">
            <div class="portlet">
                <div class="portlet-heading portlet-default">
                    <h3 class="portlet-title text-dark">
                        <i class="fa fa-building-o fa-2x "></i> company Info
                    </h3>
                    <div class="portlet-widgets">
                        <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                        <span class="divider"></span>
                        <a data-toggle="collapse" data-parent="#accordion1" href="#companyinfo"><i
                                    class="ion-minus-round"></i></a>
                        <span class="divider"></span>
                        <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="companyinfo" class="panel-collapse collapse in">
                    <div class="portlet-body">

                        {!! Form::open(['url'=>url('panel/companyinfo')]) !!}

                        <div class="form-group">
                            {!! Form::label('co_name','Company Name:') !!}
                            {!! Form::text('co_name',\App\CompanyInfo::getValueByKey('co_name'),
                            ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('address','Company Address:') !!}
                            {!! Form::text('address',\App\CompanyInfo::getValueByKey('address'),
                            ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('phone','Phone Number:') !!}
                            {!! Form::text('phone',\App\CompanyInfo::getValueByKey('phone'),['class'=>'form-control'])
                             !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('fax','company Fax:') !!}
                            {!! Form::text('fax',\App\CompanyInfo::getValueByKey('fax'),['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('email','Company Email:') !!}
                            {!! Form::email('email',\App\CompanyInfo::getValueByKey('email'),['class'=>'form-control'])
                             !!}
                        </div>

                        <div class="form-group">
                            <button class="btn btn-purple waves-effect waves-light" type="submit"> update</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>


        {{--
        *************************************->
        other Settings
        **********************************->
        --}}

        <div class="col-xs-12">
            <div class="portlet">
                <div class="portlet-heading portlet-default">
                    <h3 class="portlet-title text-dark">
                        <i class="fa fa-cog fa-2x "></i> Other Settings
                    </h3>
                    <div class="portlet-widgets">
                        <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                        <span class="divider"></span>
                        <a data-toggle="collapse" data-parent="#accordion1" href="#otherSettings"><i
                                    class="ion-minus-round"></i></a>
                        <span class="divider"></span>
                        <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="otherSettings" class="panel-collapse collapse in">
                    <div class="portlet-body">

                        {!! Form::open(['url'=>url('panel/otherSettings'),'files'=>true]) !!}

                        <div class="form-group">
                            {!! Form::label('tax','Set Default Service Tax:') !!}
                            {!! Form::text('tax',\App\CompanyInfo::getValueByKey('tax'),
                            ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('Master_invoice','Set Master Invoice Number:') !!}
                            {!! Form::text('Master_invoice',\App\CompanyInfo::getValueByKey('Master_invoice'),
                            ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('shipment_invoice','Set Shipment Invoice Number:') !!}
                            {!! Form::text('shipment_invoice',\App\CompanyInfo::getValueByKey('shipment_invoice'),['class'=>'form-control'])
                             !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('awb','Set Default AWB no.') !!}
                            {!! Form::text('awb',\App\CompanyInfo::getValueByKey('awb'),['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('sel_currency','Set Default Currency:') !!}
                            <select name="sel_currency" class="form-control">
                                <option value="0">₨</option>
                                <option value="1" selected="selected">$</option>
                                <option value="2">€</option>
                                <option value="3">£</option>
                            </select>
                        </div>

                        <div class="form-group">
                            {!! Form::label('customer_commission','Set Customer Commission%:') !!}
                            {!! Form::text('customer_commission',\App\CompanyInfo::getValueByKey('customer_commission'),
                            ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('agent_commission','Set Agent Default Commission%:') !!}
                            {!! Form::text('agent_commission',\App\CompanyInfo::getValueByKey('agent_commission'),
                            ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('delivery_days','Expected Number Of Days Delivery:') !!}
                            {!! Form::text('delivery_days',\App\CompanyInfo::getValueByKey('delivery_days'),
                            ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('logo','Logo:') !!}
                            {!! Form::file('logo',null,['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <img src="{{ asset(\App\CompanyInfo::getValueByKey('logo')) }}" alt="" class="logo"
                                 style="width: 150px; height: 150px;">
                        </div>

                        <div class="form-group">
                            <button class="btn btn-purple waves-effect waves-light" type="submit"> update</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>


    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('.logo').change(function () {
                var image = $(this).attr('src').val();
                $('.logo').attr(src, image);
            });
        });
    </script>
@stop