@extends('panel.template')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Company Details</h4>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Admin</a>
                </li>
                <li>
                    <a href="#">General Setting</a>
                </li>
                <li class="active">
                    Social Links Settings
                </li>
            </ol>
        </div>

        @if($errors->any())
            @include('errors.list')
        @endif
        {{--
        *************************************->
       facebook Settings
        **********************************->
        --}}

        <div class="col-xs-12">
            <div class="portlet">
                <div class="portlet-heading portlet-default">
                    <h3 class="portlet-title text-dark">
                        Social Settings
                    </h3>
                    <div class="portlet-widgets">
                        <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                        <span class="divider"></span>
                        <a data-toggle="collapse" data-parent="#accordion1" href="#facebook"><i
                                    class="ion-minus-round"></i></a>
                        <span class="divider"></span>
                        <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="facebook" class="panel-collapse collapse in">
                    <div class="portlet-body">
                        {!! Form::open(['url'=>'panel/social']) !!}
                        <div class="form-group">
                            <i class="fa  fa-facebook-square fa-2x"></i>
                            {!! Form::label('facebook','facebook Page Name:') !!}
                            {!! Form::text('facebook',\App\Social::getValueByKey('facebook'),['class'=>'form-control'])
                             !!}
                        </div>
                        <div class="form-group">
                            Availability :
                            <label><input type="radio" name="f_available" value="1">on</label>
                            <label><input type="radio" name="f_available" value="0">off</label>
                        </div>
                        <hr>
                        {{-- twitter Page --}}
                        <div class="form-group">
                            <i class="fa  fa-twitter-square fa-2x"></i>
                            {!! Form::label('twitter','Twitter Page Name:') !!}
                            {!! Form::text('twitter',\App\Social::getValueByKey('twitter'),['class'=>'form-control'])
                             !!}
                        </div>
                        <div class="form-group">
                            Availability :
                            <label><input type="radio" name="t_available" value="1">on</label>
                            <label><input type="radio" name="t_available" value="0">off</label>
                        </div>
                        <hr>
                        {{-- youtube page --}}
                        <div class="form-group">
                            <i class="fa fa-youtube-square fa-2x"></i>
                            {!! Form::label('youtube','youtube Page Name:') !!}
                            {!! Form::text('youtube',\App\Social::getValueByKey('youtube'),['class'=>'form-control'])
                             !!}
                        </div>
                        <div class="form-group">
                            Availability :
                            <label><input type="radio" name="y_available" value="1">on</label>
                            <label><input type="radio" name="y_available" value="0">off</label>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-purple waves-effect waves-light" type="submit"> update</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('.logo').change(function () {
                var image = $(this).attr('src').val();
                $('.logo').attr(src, image);
            });
        });
    </script>
@stop