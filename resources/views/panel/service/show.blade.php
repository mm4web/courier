@extends('panel.location_template')

@section('content')

    <style type="text/css">
        .action a {
            border-radius: 50%;
            width: 35px;
            height: 35px;
            margin-bottom: 5px;
        }

        .edit {
            float: left;
            margin-right: 10px;
        }
    </style>
    <!--Custom Toolbar-->
    <!--===================================================-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b> delivery service </b></h4>
                @if(Session::has('service_status'))
                    <p class="text-muted font-13 alert alert-success">
                        {{ session('service_status') }}
                    </p>
                @endif
                @if($errors->any())
                    @include('errors.list')
                @endif
                <table id="demo-custom-toolbar" data-toggle="table"
                       data-toolbar="#demo-delete-row"
                       data-search="true"
                       data-show-refresh="true"
                       data-show-toggle="true"
                       data-show-columns="true"
                       data-sort-name="id"
                       data-page-list="[5, 10, 20]"
                       data-page-size="5"
                       data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                    <thead>
                    <tr>
                        <th data-field="id" data-sortable="true"> sr .No</th>
                        <th data-field="city" data-sortable="true">service name</th>
                        <th data-field="status" data-sortable="true">status</th>
                        <th data-field="action" data-sortable="true">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($services as $count => $service)
                        <tr>
                            <td> {{ $count }} </td>
                            <td> {{ $service->name }} </td>
                            <td>
                                @if($service->status ==1)
                                    {!! Form::open(['url'=>url('panel/service/status/'.$service->status.'/'.$service->id)]) !!}
                                    <input type="submit" class="btn btn-xs btn-primary" value="active">
                                    {!! Form::close() !!}
                                @else
                                    {!! Form::open(['url'=>url('panel/service/status/'.$service->status.'/'.$service->id)]) !!}
                                    <input type="submit" class="btn btn-xs btn-danger" value="unactive">
                                    {!! Form::close() !!}
                                @endif
                            </td>
                            <td class="action">
                                {!! Form::open(['url'=>url('panel/service/edit/'.$service->id),'class'=>'form-inline edit','method'=>'get']) !!}
                                <button type="submit" class="btn btn-primary"><i class="fa
                                fa-pencil-square-o"></i></button>
                                {!! Form::close() !!}

                                {!! Form::open(['url'=>url('panel/service/delete/'.$service->id)]) !!}
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>
        var resizefunc = [];
    </script>
@stop