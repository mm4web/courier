@extends('panel.location_template')
@section('content')
    <div class="portlet">
        <div class="portlet-heading portlet-default">
            <h3 class="portlet-title text-dark">
                Add Service
            </h3>
            <div class="portlet-widgets">
                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                <span class="divider"></span>
                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i
                            class="ion-minus-round"></i></a>
                <span class="divider"></span>
                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div id="bg-default" class="panel-collapse collapse in">
            <div class="portlet-body">
                @if($errors->any())
                    @include('errors.list')
                @endif

                @if(Session::has('update_service'))
                    <div class="alert alert-success"> {{ session('update_service') }} </div>
                @endif
                {!! Form::model($service) !!}
                <div class="form-group">
                    {!! Form::label('name','service Name') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    <select name="status" id="status" class="form-control">
                        @if($service->status == 1)
                            <option value="1" selected> Active</option>
                        @endif
                        <option value="0"> unActive</option>
                    </select>
                </div>
                <div class="form-group">
                    {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop