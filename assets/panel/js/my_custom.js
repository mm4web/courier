/**
 * Created by mekadoo on 7/10/16.
 */
function getCity(url, target_id, country_id) {
    var url = url;
    var value = country_id;
    var target = target_id;
    $.ajax({
        url: url,
        data: {"country_id": value},
        success: function (data) {
            $(target).empty().append('<option value=""> -- select one -- </option>');
            $(data.data).each(function (data, item) {
                $(target).append('<option value="' + item.id + '">' + item.name + '</option>')
            })
        }
    })
}
