<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hubs extends Model
{
    //
    protected $table = 'hubs';
    protected $fillable = ['name', 'manager', 'address', 'phone', 'fax', 'email', 'country_id', 'branch_id', 'username',
        'password'];
}
