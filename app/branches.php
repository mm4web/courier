<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class branches extends Model
{
    //
    protected $table = 'branches';
    protected $fillable = ['city_id', 'person', 'address', 'contact_number', 'email', 'status'];

    public static function getBranch($id)
    {
        return static::where('id',$id)->first();
    }
}
