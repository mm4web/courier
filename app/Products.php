<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    protected $table = 'products';
    protected $fillable = ["name", "start_range", "end_range", "weight", "height", "width", 'length'];
}
