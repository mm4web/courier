<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PinCodes extends Model
{
    //
    protected $table = 'pincodes';
    protected $fillable = ['country_id', 'state_id', 'city_id', 'pincode'];
    
}
