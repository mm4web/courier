<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
    //
    protected $table = 'companyInfo';

    public static function findOrCreate($key)
    {
        $model = static::where('key',$key)->first();
        return $model ?: new static;
    }

    public static function getValueByKey($key)
    {
        $config = static::where('key',$key)->first();
        if ($config)
            return $config->value;
    }
}
