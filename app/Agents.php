<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agents extends Model
{
    //
    protected $table = 'agents';
    protected $fillable = ['agent_code', 'name', 'address', 'phone', 'fax', 'email', 'commission', 'country_id',
        'branch_id',
        'username',
        'password',
        'zip',
        'code'
    ];
}
