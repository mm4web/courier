<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


//Route::group(['middleware' => ['auth']], function () {

    Route::get('/panel', 'Panel\PanelController@index');
    Route::get('panel/c_details', 'Panel\GeneralSettingController@companyDetails');

//******************************* company info settings
    Route::post('panel/companyinfo', 'Panel\GeneralSettingController@companyInfo');
    Route::post('panel/otherSettings', 'Panel\GeneralSettingController@companyInfo');
//******************************* Social Settings Routes
    route::get('panel/social', 'Panel\GeneralSettingController@social');
    route::post('panel/social', 'Panel\GeneralSettingController@storeSocial');
//******************************* Shipment Routes
    Route::get('panel/new_shipment', 'Panel\ShipmentController@newShipment');
    Route::get('panel/undelivered_shipment', 'Panel\ShipmentController@undeliveredShipment');
    Route::get('panel/delivered_shipment', 'Panel\ShipmentController@deliveredShipment');
    Route::get('panel/csv-import', 'Panel\ShipmentController@importCsv');
    Route::get('panel/booking_shipment_list/all', 'Panel\ShipmentController@listAllBookingShipment');
    Route::get('panel/new_shipment/agent_code', 'Panel\ShipmentController@getAgent');
//******************************* pickup routes
    Route::get('panel/pickup/generate', 'Panel\PickupController@generatePickup');
    Route::get('panel/pickup/runningList', 'Panel\PickupController@runningList');
    Route::get('panel/pickup/completedList', 'Panel\PickupController@completedList');

//******************************* manifest Routes
    Route::get('panel/manifest/new', 'Panel\ManifestController@newManifest');
    Route::get('panel/manifest/pending', 'Panel\ManifestController@pending');
    Route::get('panel/manifest/received', 'Panel\ManifestController@received');

//******************************* DRS Routes
    Route::get('panel/drs/add', 'Panel\DrsController@AddDrs');
    Route::get('panel/drs/completed', 'Panel\DrsController@completed');
    Route::get('panel/drs/running', 'Panel\DrsController@running');

//******************************* Cod Routes
    Route::get('panel/cod/delivered', 'Panel\CODController@delivered');
    Route::get('panel/cod/undelivered', 'Panel\CODController@undelivered');

//******************************* Global received Routes
    Route::get('panel/global_received/pickup', 'Panel\GlobalReceivedController@pickup');
    Route::get('panel/global_received/manifest', 'Panel\GlobalReceivedController@manifest');

//******************************* Routes management
    Route::get('panel/route/add', 'Panel\RoutesController@add');
    Route::get('panel/route/getCities', 'Panel\RoutesController@getCities');
    Route::post('panel/route/add', 'Panel\RoutesController@store');
    Route::get('panel/route/show', 'Panel\RoutesController@show');

//******************************* Location management routes
    Route::get('panel/location/add_country', 'Panel\LocationManagementController@addCountry');
    Route::post('panel/location/add_country', 'Panel\LocationManagementController@storeCountry');

    Route::get('panel/location/add_state', 'Panel\LocationManagementController@addState');
    Route::post('panel/location/add_state', 'Panel\LocationManagementController@storeState');

    Route::get('panel/location/add_city', 'Panel\LocationManagementController@addCity');
    Route::get('panel/location/getStates', 'Panel\LocationManagementController@getState');
    Route::post('panel/location/add_city', 'Panel\LocationManagementController@storeCity');


    Route::get('panel/location/add_pincode', 'Panel\LocationManagementController@addPincode');
    Route::get('panel/location/getCities', 'Panel\LocationManagementController@getCities');
    Route::post('panel/location/add_pincode', 'Panel\LocationManagementController@storePinCode');
    Route::get('panel/location/pincode/list', 'Panel\LocationManagementController@listPincode');
    Route::post('panel/pincod/delete/{id}', 'Panel\LocationManagementController@deletePincode');
    Route::get('panel/pincod/edit/{id}', 'Panel\LocationManagementController@editPincode');
    Route::post('panel/pincod/edit/{id}', 'Panel\LocationManagementController@updatePincode');

    Route::get('panel/location/list', 'Panel\LocationManagementController@listlocations');
    Route::post('panel/location/delete/{id}', 'Panel\LocationManagementController@deleteLocation');
    Route::get('panel/location/edit/{id}', 'Panel\LocationManagementController@editLocation');
    Route::post('panel/location/edit/{id}', 'Panel\LocationManagementController@updateLocation');


//******************************* service routes
    Route::get('panel/service/add', 'Panel\ServiceController@add');
    Route::post('panel/service/add', 'Panel\ServiceController@store');
    Route::get('panel/service/show', 'Panel\ServiceController@show');
    Route::post('panel/service/status/{status}/{id}', 'Panel\ServiceController@status');
    Route::post('panel/service/delete/{id}', 'Panel\ServiceController@delete');
    Route::get('panel/service/edit/{id}', 'Panel\ServiceController@edit');
    Route::post('panel/service/edit/{id}', 'Panel\ServiceController@update');

//******************************* Product routes

    Route::get('panel/product/all', 'Panel\ProductController@index');
    Route::get('panel/product/add', 'Panel\ProductController@newProduct');
    Route::post('panel/product/add', 'Panel\ProductController@storeProduct');
    Route::get('panel/product/edit/{id}', 'Panel\ProductController@editProduct');
    Route::post('panel/product/edit/{id}', 'Panel\ProductController@updateProduct');
    Route::post('panel/product/delete/{id}', 'Panel\ProductController@deleteProduct');

//******************************* users management routes
    Route::get('panel/users-management/add_agent', 'Panel\AgentManagementController@addAgent');
    Route::post('panel/users-management/add_agent', 'Panel\AgentManagementController@storeAgent');
    Route::get('panel/users-management/show_agents', 'Panel\AgentManagementController@showAgents');
    Route::get('panel/agent/edit/{id}', 'Panel\AgentManagementController@editAgent');
    Route::post('panel/agent/edit/{id}', 'Panel\AgentManagementController@updateAgent');
    Route::post('panel/agent/delete/{id}', 'Panel\AgentManagementController@deleteAgent');

    Route::get('panel/users-management/add_hub', 'Panel\HubManagementController@addHub');
    Route::post('panel/users-management/add_hub', 'Panel\HubManagementController@storeHub');
    Route::get('panel/users-management/show_hubs', 'Panel\AgentManagementController@showHubs');
//******************************* staff routes
    Route::get('panel/staff/add', 'Panel\StaffController@add');
    Route::post('panel/staff/add', 'Panel\StaffController@storeStaff');
    Route::get('panel/staff/edit/{id}', 'Panel\StaffController@editStaff');
    Route::post('panel/staff/edit/{id}', 'Panel\StaffController@updateStaff');
    Route::post('panel/staff/delete/{id}', 'Panel\StaffController@deleteStaff');
    Route::get('panel/staff/all', 'Panel\StaffController@showAll');
//******************************* customer management routes
    Route::get('panel/customer/add', 'Panel\CustomerController@newCustomer');
    Route::post('panel/customer/add', 'Panel\CustomerController@storeCustomer');

    Route::get('panel/customer/edit/{id}', 'Panel\CustomerController@editCustomer');
    Route::post('panel/customer/edit/{id}', 'Panel\CustomerController@updateCustomer');

    Route::get('panel/customer/show', 'Panel\CustomerController@show');
//******************************* messenger Routes
    Route::get('panel/messenger/add', 'Panel\MessengerController@add');
    Route::post('panel/messenger/add', 'Panel\MessengerController@store');
    Route::get('panel/messenger/getBranches', 'Panel\MessengerController@getBranches');
    Route::get('panel/messenger/all', 'Panel\MessengerController@showAll');
    Route::get('panel/messenger/edit/{id}', 'Panel\MessengerController@editMessenger');
    Route::post('panel/messenger/edit/{id}', 'Panel\MessengerController@updateMessenger');
    Route::post('panel/messenger/delete/{id}', 'Panel\MessengerController@deleteMessenger');
    Route::get('panel/messenger/monitor', 'Panel\MessengerController@monitor');

//******************************* branch route
    Route::get('panel/branch/add', 'Panel\BranchController@add');
    Route::post('panel/branch/add', 'Panel\BranchController@store');
    Route::get('panel/branch/show', 'Panel\BranchController@show');
    Route::post('panel/branch/status/{status}/{id}', 'Panel\BranchController@status');
    Route::post('panel/branch/delete/{id}', 'Panel\BranchController@delete');
    Route::get('panel/branch/edit/{id}', 'Panel\BranchController@edit');
    Route::post('panel/branch/edit/{id}', 'Panel\BranchController@update');

//******************************* get city routes
    Route::get('panel/locations/getCity', 'Panel\PanelController@getCity');
    Route::get('panel/locations/getBranch', 'Panel\PanelController@getBranch');

//******************************* Zone controller routes

    Route::get('panel/zones/list', 'Panel\ZoneController@index');
    Route::post('panel/zones/list', 'Panel\ZoneController@newZone');
    Route::get('panel/zones/delete/{id}', 'Panel\ZoneController@deleteZone');

//});

Route::get('/home', 'HomeController@index');
//Route::auth();


