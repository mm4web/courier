<?php

namespace App\Http\Controllers\Panel;

use App\Cities;
use App\Countries;
use App\Http\Requests\CreateRouteRequest;
use App\Routes;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class RoutesController extends Controller
{
    //
    public function add()
    {
        $countries = Countries::all();
        return view('panel.routes.add', compact('countries'));
    }

    public function getCities(Request $request)
    {
        $cities = Cities::where('country_id', $request->country_id)->get();
        return response()->json(['success' => true, 'cities' => $cities]);
    }

    public function store(CreateRouteRequest $request)
    {
        $routes = new Routes();
        $routes->create($request->all());
        Session::flash('new_route', 'Routes added Successfully');
        return redirect()->back();
    }

    public function show()
    {
        $routes = Routes::groupBy('name')->get();
        return view('panel.routes.show', compact('routes'));
    }
}
