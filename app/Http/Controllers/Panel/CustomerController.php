<?php

namespace App\Http\Controllers\Panel;

use App\Cities;
use App\Countries;
use App\Customers;
use App\Http\Requests\CreateNewCustomerRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    //
    public function newCustomer()
    {
        $countries = Countries::all();
        $cities = Cities::all();
        return view('panel.customer.add', compact('countries', 'cities'));
    }

    public function storeCustomer(CreateNewCustomerRequest $request)
    {
        $customer = new Customers();

        $customer->create($request->all());
        Session::flash('new_customer', 'Customer Added Successfully');
        return redirect()->back();
    }

    public function editCustomer($id)
    {
        $countries = Countries::all();
        $customer = Customers::find($id);
        return view('panel.customer.edit', compact('customer', 'countries'));
    }

    public function show()
    {
        $customers = Customers::all();
        return view('panel.customer.show', compact('customers'));
    }
}
