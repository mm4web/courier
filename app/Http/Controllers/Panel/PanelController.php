<?php

namespace App\Http\Controllers\Panel;

use App\branches;
use App\Cities;
use App\Countries;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Gate;
use Illuminate\Support\Facades\Auth;

class PanelController extends Controller
{
    //
    public function index()
    {
        return view('panel.index');
    }

    public function getCity(Request $request)
    {
        $cities = Cities::where('country_id', $request->country_id)->get();
        return response()->json(['success' => true, 'data' => $cities]);
    }   

//    public function getBranch(Request $request)
//    {
//
//        $branches = branches::where('country_id', $request->country_id)->get();
//        return response()->json(['success' => true, 'branches' => $branches]);
//    }
}
