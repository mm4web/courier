<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DrsController extends Controller
{
    //
    public function addDrs()
    {
        return view('panel.drs.add');
    }

    public function completed()
    {
        return view('panel.drs.completed');
    }

    public function running()
    {
        return view('panel.drs.running');
    }
}
