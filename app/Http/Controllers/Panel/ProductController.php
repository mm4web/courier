<?php

namespace App\Http\Controllers\Panel;

use App\Products;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Session;

class ProductController extends Controller
{
    //

    public function index()
    {
        $products = Products::all();
        return view('panel.products.all', compact('products'));
    }

    public function newProduct()
    {
        return view('panel.products.new');
    }

    public function storeProduct(Request $request)
    {
        dd($request->all());
        $rules = [
            'name' => 'required',
            'weight' => 'required'
        ];

        /*if ($request->input('dimensions') == '1') {
            foreach ($request->get('start_range') as $key => $val) {
                $rules['start_range.' . $key] = 'required|min:0';
            }
            foreach ($request->get('end_range') as $key => $val) {
                $rules['end_range.' . $key] = 'required|min:0';
            }
        }*/

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $product = new Products();
        $product->name = $request->input('name');
        $start = implode(',', $request->input('start_range'));
        $product->start_range = $start;
        $end = implode(',', $request->input('end_range'));
        $product->end_range = $end;
        $product->weight = $request->input('weight');
        if ($request->input('dimensions') == '1') {
            $product->height = $request->input('height');
            $product->width = $request->input('width');
            $product->length = $request->input('length');
        }

        $product->save();
        Session::flash('new_product', 'Product created successfully');
        return redirect()->back();

    }

    public function editProduct($id)
    {
        $product = Products::find($id);
        $start_range = explode(',', $product->start_range);
        $end_range = explode(',', $product->end_range);
        return view('panel.products.edit', compact('product', 'start_range', 'end_range'));
    }
}
