<?php

namespace App\Http\Controllers\Panel;

use App\CompanyInfo;
use App\Http\Requests\CompanyInfoNewRequest;
use App\Http\Requests\SocialRequest;
use App\Social;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class GeneralSettingController extends Controller
{
    //


    public function companyDetails()
    {
        $logo = CompanyInfo::getValueByKey('logo');
        if ($logo) {
            return view('panel.generalSetting.company', compact('logo'));
        } else {
            return view('panel.generalSetting.company');
        }
    }

    public function companyInfo(CompanyInfoNewRequest $request)
    {
        $inputs = Input::except('_token');
        foreach ($inputs as $key => $value) {
            $info = CompanyInfo::findOrCreate($key);
            $info->key = $key;
            if ($value instanceof UploadedFile) {
                $file = Input::file($key);
                $destinationPath = 'assets/site/images';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $info->value = $destinationPath . '/' . $filename;
            } else {
                $info->value = $value;
            }
            $info->save();
        }
        return redirect()->back();
    }

    public function social()
    {
        return view('panel.generalSetting.social');
    }

    public function storeSocial(SocialRequest $request)
    {
        $inputs = Input::except('_token');
        foreach ($inputs as $key => $value) {
            $info = Social::findOrCreate($key);
            $info->key = $key;
            if ($value instanceof UploadedFile) {
                $file = Input::file($key);
                $destinationPath = 'assets/site/images';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $info->value = $destinationPath . '/' . $filename;
            } else {
                $info->value = $value;
            }
            $info->save();
        }
        return redirect()->back();
    }
}
