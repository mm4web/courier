<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class CODController extends Controller
{
    //
    public function delivered()
    {
        return view('panel.cod.delivered');
    }

    public function undelivered()
    {
        return view('panel.cod.undelivered');
    }
}
