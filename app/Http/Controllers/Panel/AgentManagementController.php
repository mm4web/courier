<?php

namespace App\Http\Controllers\Panel;

use App\Agents;
use App\branches;
use App\Countries;
use App\Http\Requests\CreateAgentRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;

class AgentManagementController extends Controller
{
    //
    public function addAgent()
    {
        $randCode = ''; //mt_rand($min = 1111, $max = 9999);
        for ($i = 0; $i < 4; $i++) {
            $randCode .= mt_rand(0, 9);
        }
        $countries = Countries::all();
        $branches = branches::all();
        return view('panel.users.newAgent', compact('countries', 'randCode', 'branches'));
    }

    public function storeAgent(CreateAgentRequest $request)
    {
        $randCode = ''; //mt_rand($min = 1111, $max = 9999);
        for ($i = 0; $i < 4; $i++) {
            $randCode .= mt_rand(0, 9);
        }

        $agent = new Agents();
        $data = [
            'name' => $request->input('name'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'fax' => $request->input('phone'),
            'email' => $request->input('email'),
            'commission' => $request->input('commission'),
            'country_id' => $request->input('country_id'),
            'branch_id' => $request->input('branch_id'),
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
            'zip' => $request->input('zip'),
            'agent_code' => $request->input('agent_code'),
            'code' => 'tr' . $randCode
        ];

        $agent->create($data);
        Session::flash('new_agent', 'Agent added successfully');
        return redirect()->back();
    }


    public function showAgents()
    {
        $agents = Agents::all();

        return view('panel.users.showAgents', compact('agents'));
    }

    public function editAgent($id)
    {
        $agent = Agents::find($id);
        $countries = Countries::all();
        $branches = branches::all();
        return view('panel.users.editAgent', compact('agent', 'countries', 'branches'));
    }

    public function updateAgent(Request $request, $id)
    {

        $agent = Agents::find($id);
        $rules = [
            "name" => "required|min:5",
            "address" => "required|min:5",
            "phone" => "required|min:5|numeric",
            "fax" => "required|numeric",
            "email" => "required|email",
            "commission" => "required|numeric",
            "country_id" => "required|numeric",
            "branch_id" => "required|numeric",
            "username" => "required|min:5",
            'agent_code' => 'required|min:4',
        ];
        if (!empty($request->input('password'))) {
            $rules['password'] = "required|min:6|confirmed";
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = [
            'name' => $request->input('name'),
            'manager' => $request->input('manager'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'fax' => $request->input('fax'),
            'email' => $request->input('email'),
            'country_id' => $request->input('country_id'),
            'branch_id' => $request->input('branch_id'),
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
            'agent_code' => 'tr' . $request->input('agent_code')
        ];

        $agent->update($data);
        Session::flash('update_agent', 'agent updated successfully');
        return redirect()->back();
    }

    public function deleteAgent($id)
    {
        $agent = Agents::find($id);
        $agent->delete();
        Session::flash('delete_agent', 'Agent Deleted Successfully');
        return redirect()->back();
    }
}
