<?php

namespace App\Http\Controllers\Panel;

use App\Http\Requests\CreatStaffRequest;
use App\Http\Requests\EditStaffRequest;
use App\Staff;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Session;

class StaffController extends Controller
{
    //
    public function add()
    {
        return view('panel.staff.add');
    }

    public function storeStaff(CreatStaffRequest $request)
    {
        $staff = new Staff();
        $data = [
            'name' => $request->input('name'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password'))
        ];
        $staff->create($data);
        Session::flash('new_staff', 'staff added successfully');
        return redirect()->back();
    }

    public function editStaff($id)
    {
        $staff = Staff::find($id);
        return view('panel.staff.edit', compact('staff'));
    }

    public function updateStaff(EditStaffRequest $request, $id)
    {

        $staff = Staff::find($id);
        $data = [
            'name' => $request->input('name'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'username' => $request->input('username'),
        ];
        if (!empty($request->input('password'))) {
            $data = [
                'name' => $request->input('name'),
                'address' => $request->input('address'),
                'phone' => $request->input('phone'),
                'username' => $request->input('username'),
                'password' => bcrypt($request->input('password'))
            ];
        }

        $staff->update($data);
        Session::flash('update_staff', 'staff updated successfully');
        return redirect()->back();
    }

    public function showAll()
    {
        $staffs = Staff::all();
        return view('panel.staff.all', compact('staffs'));
    }

    public function deleteStaff($id)
    {
        $staff = Staff::find($id);
        $staff->delete();
        Session::flash('delete_staff', 'Staff deleted Successfully');
        return redirect()->back();
    }

}
