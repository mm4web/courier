<?php

namespace App\Http\Controllers\Panel;

use App\Services;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;


class ServiceController extends Controller
{
    //
    public function add()
    {
        return view('panel.service.add');
    }

    public function store()
    {
        $service = new Services();
        $rules = array('name' => 'required', 'status' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $message = $validator->messages();
            return redirect()->back()->withErrors($validator);
        }
        $service->name = Input::get('name');
        $service->status = Input::get('status');
        $service->save();
        Session::flash('new_service', 'New service Added  Successfully');
        return redirect()->back();
    }

    public function edit($id)
    {
        $service = Services::find($id);
        return view('panel.service.edit', compact('service'));
    }

    public function update($id)
    {
        $service = Services::find($id);
        $rules = array('name' => 'required', 'status' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $message = $validator->messages();
            return redirect()->back()->withErrors($validator);
        }
        $service->name = Input::get('name');
        $service->status = Input::get('status');
        $service->save();
        Session::flash('update_service', 'service updated Successfully');
        return redirect()->back();
    }

    public function show()
    {
        $services = Services::all();
        return view('panel.service.show', compact('services'));
    }

    public function status($status, $id)
    {
        $service = Services::find($id);
        if ($status == 1) {
            $service->status = '0';
            $service->save();
            Session::flash('service_status', 'Service now is hidden');
            return redirect()->back();
        }
        $service->status = '1';
        $service->save();
        Session::flash('service_status', 'Service now is visible');
        return redirect()->back();
    }

    public function delete($id)
    {
        $service = Services::find($id);
        $service->delete();
        Session::flash('delete_service', 'service Deleted successfully ');
        return redirect()->back();
    }
}
