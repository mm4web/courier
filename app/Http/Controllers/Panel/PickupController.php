<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PickupController extends Controller
{
    //
    public function generatePickup()
    {
        return view('panel.pickup.generate');
    }

    public function runningList()
    {
        return view('panel.pickup.runningList');
    }

    public function completedList()
    {
        return view('panel.pickup.completedList');
    }
}
