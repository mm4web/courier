<?php

namespace App\Http\Controllers\Panel;

use App\branches;
use App\Cities;
use App\Countries;
use App\Http\Requests\CreateMessengerRequest;
use App\Http\Requests\editMessengersRequest;
use App\Messengers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Session;
use Illuminate\Support\Facades\File;

class MessengerController extends Controller
{
    //
    public function add()
    {
        $branches = branches::all();
        $cities = Cities::all();
        $countries = Countries::all();
        return view('panel.messenger.add', compact('branches', 'cities', 'countries'));
    }


    public function getBranches(Request $request)
    {
        $brunches = branches::where('city_id', $request->city_id)->get();
        return response()->json(['success' => true, 'branches' => $brunches]);
    }

    public function store(CreateMessengerRequest $request)
    {
        $messenger = new Messengers();

        if (Input::hasFile('image')) {
            $file = $request->file('image');
            $destination = 'assets/Panel/messengers';
            $fileName = $file->getClientOriginalName();
            $file->move($destination, $fileName);
            $image = $destination . '/' . $fileName;
            $messenger->image = $image;
        }

        $messenger->name = $request->input('name');
        $messenger->code = $request->input('code');
        $messenger->branch_id = $request->input('branch_id');
        $messenger->city_id = $request->input('city_id');
        $messenger->country_id = $request->input('country_id');
        $messenger->mobil = $request->input('mobil');
        $messenger->email = $request->input('email');
        $messenger->password = bcrypt($request->input('password'));
        $messenger->vehicle_number = $request->input('vehicle_number');

        $messenger->save();

        Session::flash('new_messenger', 'Messenger Added successfully');
        return redirect()->back();

    }

    public function showAll()
    {
        $messengers = Messengers::all();
        return view('panel.messenger.all', compact('messengers'));
    }

    public function editMessenger($id)
    {
        $messenger = Messengers::find($id);
        $branches = branches::all();
        $cities = Cities::all();
        $countries = Countries::all();
        return view('panel.messenger.edit', compact('messenger', 'branches', 'cities', 'countries'));
    }

    public function updateMessenger(editMessengersRequest $request, $id)
    {
        $messenger = Messengers::find($id);
        if (Input::hasFile('image')) {
            $file = $request->file('image');
            $destination = 'assets/Panel/messengers';
            $fileName = $file->getClientOriginalName();
            $file->move($destination, $fileName);
            $image = $destination . '/' . $fileName;
            $messenger->image = $image;
        }

        $messenger->name = $request->input('name');
        $messenger->code = $request->input('code');
        $messenger->branch_id = $request->input('branch_id');
        $messenger->city_id = $request->input('city_id');
        $messenger->country_id = $request->input('country_id');
        $messenger->mobil = $request->input('mobil');
        $messenger->email = $request->input('email');
        if (!empty($request->input('password'))) {
            $messenger->password = bcrypt($request->input('password'));
        }
        $messenger->vehicle_number = $request->input('vehicle_number');

        $messenger->save();

        Session::flash('update_messenger', 'Messenger updated successfully');
        return redirect()->back();
    }

    public function deleteMessenger($id)
    {
        $messenger = Messengers::find($id);
        $image = $messenger->image;
        File::Delete($image);
        $messenger->delete();
        Session::flash('delete_messenger', 'messenger deleted successfully');
        return redirect()->back();
    }

    public function monitor()
    {
        return view('panel.messenger.monitor');
    }
}
