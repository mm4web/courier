<?php

namespace App\Http\Controllers\Panel;

use App\Agents;
use App\Countries;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ShipmentController extends Controller
{
    //
    public function newShipment()
    {
        $countries = Countries::all();
        return view('panel.shipment.new', compact('countries'));
    }


    public function undeliveredShipment()
    {
        return view('panel.shipment.undelivered');
    }

    public function deliveredShipment()
    {
        return view('panel.shipment.delivered');
    }

    public function importCsv()
    {
        return view('panel.shipment.importCsv');
    }

    public function listAllBookingShipment()
    {
        return view('panel.shipment.listAll');
    }

    public function getAgent(Request $request)
    {
        $agent = Agents::where('agent_code', $request->agent_code)->first();
        return response()->json(['success' => true, 'user' => $agent]);
    }

}