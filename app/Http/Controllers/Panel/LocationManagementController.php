<?php

namespace App\Http\Controllers\Panel;

use App\Cities;
use App\Countries;

use App\Http\Requests\CreateCityRequest;
use App\Http\Requests\CreateCountryRequest;
use App\Http\Requests\CreatePinCodeRequest;
use App\Http\Requests\CreateStateRequest;

use App\Http\Requests\EditCountryRequest;
use App\PinCodes;
use App\States;
use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\File;

class LocationManagementController extends Controller
{
    // country functions
    public function addCountry()
    {
        return view('panel.locations.addCountry');
    }

    public function storeCountry(CreateCountryRequest $request)
    {
        $country = new Countries();
        if (Input::hasFile('image')) {
            $file = $request->file('image');
            $path = 'assets/panel/locations/countries';
            $filename = $file->getClientOriginalName();
            $file->move($path, $filename);
            $image = $path . '/' . $filename;
            $country->image = $image;
        }

        $country->country = $request->input('country');
        $country->title = $request->input('title');
        $country->description = $request->input('description');
        $country->keywords = $request->input('keywords');
        $country->save();
        Session::flash('new_country', 'new Country added successfully');
        return redirect()->back();

    }


    //*************************** states functions 

    public function addState()
    {
        $countries = Countries::all();
        return view('panel.locations.addState', compact('countries'));
    }

    public function storeState(CreateStateRequest $request)
    {
        $state = new States();
        $state->create($request->all());
        Session::flash('new_state', 'State has added successfully ');
        return redirect()->back();
    }

//*************************** City Functions
    public function getState(Request $request)
    {
        $state = States::where('country', $request->country_id)->get();
        return response()->json(['success' => true, 'data' => $state]);
    }

    public function addCity()
    {
        $countries = Countries::all();
        $states = States::all();
        return view('panel.locations.addCity', compact('countries', 'states'));
    }

    public function storeCity(CreateCityRequest $request)
    {
        $city = new Cities();
        $city->create($request->all());
        Session::flash('new_city', 'City added successfully');
        return redirect()->back();
    }

//******************************* pin code functions
    public function addPincode()
    {
        $countries = Countries::all();
        $states = States::all();
        $cities = Cities::all();
        return view('panel.locations.addPincode', compact('countries', 'states', 'cities'));
    }

    public function getCities(Request $request)
    {
        $cities = Cities::where('state_id', $request->state_id)->get();
        return response()->json(['success' => true, 'cities' => $cities]);
    }

    public function storePinCode(CreatePinCodeRequest $request)
    {
        $pincodes = PinCodes::create($request->all());
        Session::flash('new_pincode', 'Pin code added successfully');
        return redirect()->back();
    }

    public function listPincode()
    {
        $pincodes = PinCodes::all();
        return view('panel.locations.allpins', compact('pincodes'));
    }

    public function deletePincode($id)
    {
        $pin = PinCodes::find($id);
        $pin->delete();
        Session::flash('delete_pincode', 'pincode deleted successfully');
        return redirect()->back();
    }

    public function editPincode($id)
    {
        $pin = PinCodes::find($id);
        $countries = Countries::all();
        $states = States::all();
        $cities = Cities::all();
        return view('panel.locations.editPincode', compact('pin', 'countries', 'states', 'cities'));
    }

    public function updatePincode(CreatePinCodeRequest $request, $id)
    {
        $pin = PinCodes::find($id);
        $pin->update($request->all());
        Session::flash('update_pin', 'pin updated successfully');
        return redirect()->back();
    }

    public function listLocations()
    {
        $countries = Countries::all();
        return view('panel.locations.allLocations', compact('countries'));
    }

    public function editLocation($id)
    {
        $country = Countries::find($id);
        return view('panel.locations.editCountry', compact('country'));
    }

    public function updateLocation(EditCountryRequest $request, $id)
    {
        $country = Countries::find($id);

        if (Input::hasFile('image')) {
            $file = $request->file('image');
            $path = 'assets/panel/locations/countries';
            $fileName = $file->getClientOriginalName();
            $file->move($path, $fileName);
            $image = $path . '/' . $fileName;
            $country->image = $image;
        }
        $country->country = $request->input('country');
        $country->title = $request->input('title');
        $country->description = $request->input('description');
        $country->keywords = $request->input('keywords');
        $country->save();
        Session::flash('update_location', 'Location updated successfully');
        return redirect()->back();
    }

    public function deleteLocation($id)
    {
        $location = Countries::find($id);
        $image = $location->image;
        File::Delete($image);
        $location->delete();
        Session::flash('delete_location', 'Location deleted successfully');
        return redirect()->back();
    }

}
