<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ManifestController extends Controller
{
    //
    public function newManifest()
    {
        return view('panel.manifest.new');
    }

    public function pending()
    {
        return view('panel.manifest.pending');
    }

    public function received()
    {
        return view('panel.manifest.received');
    }
}

