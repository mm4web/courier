<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GlobalReceivedController extends Controller
{
    //
    public function pickup()
    {
        return view('panel.globalReceived.pickup');
    }

    public function manifest()
    {
        return view('panel.globalReceived.manifest');
    }
}
