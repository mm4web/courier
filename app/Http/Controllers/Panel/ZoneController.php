<?php

namespace App\Http\Controllers\Panel;

use App\Zones;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Session;
use Input;

class ZoneController extends Controller
{
    //
    public function index()
    {
        $zones = Zones::all();
        return view('panel.zone.list', compact('zones'));
    }

    public function newZone(Request $request)
    {
        $rules = ['name' => 'required|min:2'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400);
        }

        $zone = new Zones();
        $zone->name = $request->input('name');
        $zone->save();
        return Response::json(['success' => true, 'view' => view('panel.zone.row', compact('zone'))->render()]);
    }

    public function deleteZone($id)
    {
        $zone = Zones::find($id);
        $zone->delete();
        return Response::json(['delete' => true, 'id' => $id]);
    }
}
