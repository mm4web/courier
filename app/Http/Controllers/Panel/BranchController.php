<?php

namespace App\Http\Controllers\Panel;

use App\branches;
use App\Cities;
use App\Http\Requests\CreateBranchRequest;
use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BranchController extends Controller
{
    //
    public function add()
    {
        $cities = Cities::all();
        return view('panel.branch.add', compact('cities'));
    }

    public function store(CreateBranchRequest $request)
    {
        $branch = new branches();
        $branch->create($request->all());
        Session::flash('new_branch', 'new branch added successfully');
        return redirect()->back();

    }

    public function show()
    {
        $branches = branches::all();
        return view('panel.branch.show', compact('branches'));
    }

    public function status($status, $id)
    {
        $branch = branches::find($id);
        if ($branch->status == 1) {
            $branch->status = '0';
            $branch->save();
            Session::flash('branch_status', 'branch now is hidden');
            return redirect()->back();
        }
        $branch->status = '1';
        $branch->save();
        Session::flash('branch_status', 'branch now is visible');
        return redirect()->back();
    }

    public function edit($id)
    {
        $branch = branches::find($id);
        $cities = Cities::all();
        return view('panel.branch.edit', compact('branch', 'cities'));
    }

    public function update(CreateBranchRequest $request, $id)
    {
        $branch = branches::find($id);
        $branch->update($request->all());
        Session::flash('update_branch', ' branch updated successfully');
        return redirect()->back();
    }

    public function delete($id)
    {
        $branch = branches::find($id);
        $branch->delete();
        Session::flash('delete_branch', 'branch deleted successfully');
        return redirect()->back();
    }
}
