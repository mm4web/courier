<?php

namespace App\Http\Controllers\Panel;

use App\branches;
use App\Countries;
use App\Hubs;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Session;

class HubManagementController extends Controller
{
    //
    public function addHub()
    {
        $countries = Countries::all();
        $branches = branches::all();
        return view('panel.users.newHub', compact('countries', 'branches'));
    }

    public function storeHub(Request $request)
    {
        $hub = new Hubs();
        $rules = [
            "name" => "required|min:5",
            "manager" => "required|min:5",
            "address" => "required|min:5",
            "phone" => "required|numeric|min:9",
            "fax" => "required|numeric|min:5",
            "email" => "required|email",
            "country_id" => "required|numeric",
            "branch_id" => "required|numeric",
            "username" => "required|min:5",
            "password" => "required|min:6|confirmed"
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = [
            'name' => $request->input('name'),
            'manager' => $request->input('manager'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'fax' => $request->input('fax'),
            'email' => $request->input('email'),
            'country_id' => $request->input('country_id'),
            'branch_id' => $request->input('branch_id'),
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password'))
        ];

        $hub->create($data);
        Session::flash('new_hub', 'hub Added Successfully');
        return redirect()->back();
    }
}
