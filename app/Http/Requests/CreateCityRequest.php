<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateCityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //

            'country_id' => 'required',
            'state_id' => 'required',
            'name' => 'required|min:3',
            'title' => 'required|min:5',
            'description' => 'min:5',
            'keywords' => 'min:5'
        ];
    }
}
