<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreatStaffRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "name" => "required|min:5",
            "address" => "required|min:5",
            "phone" => "numeric|required",
            "username" => "required|min:5",
            "password" => "required|min:6|confirmed"
        ];
    }
}
