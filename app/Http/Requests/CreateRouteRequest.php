<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateRouteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "country_id" => "required",
            "city_id" => "required",
            "route_code" => "required|numeric",
            "name" => "required"
        ];
    }
}
