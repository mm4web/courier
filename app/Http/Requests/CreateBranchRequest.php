<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateBranchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'city_id' => 'required',
            'person' => 'required|min:5',
            'contact_number' => 'required|numeric',
            'email' => 'email|required'
        ];
    }
}
