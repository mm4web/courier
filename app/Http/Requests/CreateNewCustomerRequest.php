<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateNewCustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|min:5",
            "company" => "required",
            "zip" => "required|numeric",
            "country_id" => "required|numeric",
            "city_id" => "required|numeric",
            "phone" => "required|numeric",
            "address" => "required",
            "email" => "required|email",
            "customer_commission" => "required|numeric",
            "password" => "required|confirmed|min:6"
        ];
    }
}
