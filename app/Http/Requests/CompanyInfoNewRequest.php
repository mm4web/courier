<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompanyInfoNewRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'co_name' => 'min:5',
            'address' => 'min:5',
            'phone' => 'numeric|min:5',
            'fax' => 'numeric|min:5',
            'email' => 'email',
            'tax' => 'numeric',
            'Master_invoice' => 'numeric',
            'shipment_invoice' => 'numeric',
            'awb' => 'numeric',
            'sel_currency' => 'numeric',
            
        ];
    }
}
