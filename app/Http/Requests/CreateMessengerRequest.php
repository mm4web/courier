<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateMessengerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'code' => 'numeric|required',
            'branch_id' => 'numeric|required',
            'city_id' => 'numeric|required',
            'country_id' => 'numeric|required',
            'mobil' => 'numeric|required|min:9',
            'email' => 'required|email',
            'password' => 'confirmed|min:6|required',
            'vehicle_number' => 'required',
            'image' => 'required|image'
        ];
    }
}
