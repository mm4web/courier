<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateAgentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|min:3",
            "address" => "required|min:5",
            "phone" => "required|min:5|numeric",
            "fax" => "required|numeric",
            "email" => "required|email",
            "commission" => "required|numeric",
            "country_id" => "required|numeric",
            "branch_id" => "required|numeric",
            "username" => "required|min:3",
            'agent_code' => 'required|min:4|numeric',
            "password" => "required|min:6|confirmed",
            'zip' => 'required|min:5|numeric'
        ];
    }
}
