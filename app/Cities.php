<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    //
    protected $table = 'cities';
    protected $fillable = ['country_id', 'state_id', 'name', 'title', 'description', 'keywords'];

    public static function getCityName($id)
    {
        return static::where('id', $id)->first()->name;
    }
}
