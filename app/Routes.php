<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routes extends Model
{
    //
    protected $table = 'routes';
    protected $fillable = ['country_id', 'city_id', 'route_code', 'name'];
}
