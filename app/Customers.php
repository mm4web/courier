<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customers';
    protected $fillable = ['name', 'company', 'zip', 'country_id', 'city_id', 'phone', 'address', 'email',
        'customer_commission',
        'password'];
}
