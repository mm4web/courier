<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messengers extends Model
{
    //
    protected $table = 'messengers';
//"name" => "Reese Wilkinson"
//"code" => "Ex consectetur ex consequatur similique mollit dolor commodi nihil ipsum earum aliquam officia excepturi possimus soluta velit suscipit"
//"branch" => "Quibusdam assumenda eiusmod iste sed deserunt dolores"
//"country" => "6"
//"city" => "2"
//"mobil" => "23"
//"email" => "xivoz@gmail.com"
//"password" => "Pa$$w0rd!"
//"password_confirmation" => "Pa$$w0rd!"
//"Vehicle" => "35"
//"image"
    protected $fillable = [
        'name', 'code', 'branch_id', 'country_id', 'city_id', 'mobil', 'email', 'password',
        'vehicle_number', 'image'
    ];
}
